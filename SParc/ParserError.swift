
open class ParserError: Error {
    public class Expected: ParserError {
        public let label: String
        
        public init(_ label: String) {
            self.label = label
            super.init()
        }
    }
    
    public class ExpectedString: ParserError {
        public let string: String
        public let caseInsensitive: Bool
        
        public init(_ string: String, caseInsensitive: Bool = false) {
            self.string = string
            self.caseInsensitive = caseInsensitive
            super.init()
        }
    }
    
    public class Unexpected: ParserError {
        public let label: String
        
        public init(_ label: String) {
            self.label = label
            super.init()
        }
    }
    
    public class UnexpectedString: ParserError {
        public let string: String
        public let caseInsensitive: Bool
        
        public init(_ string: String, caseInsensitive: Bool = false) {
            self.string = string
            self.caseInsensitive = caseInsensitive
            super.init()
        }
    }
    
    public class Other: ParserError {
        public let message: String
        
        public init(message: String) {
            self.message = message
            super.init()
        }
    }
}

public class NestedParserError: ParserError {
    let position: CharacterStream.Position
    let userInfo: CharacterStream.UserInfo
    let errors: [ParserError]
    
    public init(position: CharacterStream.Position, userInfo: CharacterStream.UserInfo,
                errors: [ParserError])
    {
        self.position = position
        self.userInfo = userInfo
        self.errors = errors
    }
}

public class CompoundParserError: ParserError {
    let position: CharacterStream.Position
    let userInfo: CharacterStream.UserInfo
    let errors: [ParserError]
    let label: String
    
    public init(position: CharacterStream.Position, userInfo: CharacterStream.UserInfo,
                errors: [ParserError], label: String)
    {
        self.position = position
        self.userInfo = userInfo
        self.errors = errors
        self.label = label
    }
}



