
open class CharacterStream {
    public typealias Index = String.Index
    public typealias UserInfo = [String: Any]
    
//    public static let endOfStreamChar: Character = "\u{FFFF}"
    
    internal let string: String
    open let startIndex: Index
    open let endIndex: Index
    open var stateTag: Int = 0
    open fileprivate(set) var nextIndex: Index
    open fileprivate(set) var lineNumber: Int = 1
    open fileprivate(set) var lineStartIndex: Index
    open var userInfo: UserInfo = [:] {
        didSet { stateTag += 1 }
    }
    open var name: String = "" {
        didSet { stateTag += 1 }
    }
    
    internal init(string: String, startIndex: Index, endIndex: Index) {
        precondition(string.startIndex <= startIndex)
        precondition(string.endIndex >= endIndex)
        self.string = string
        self.startIndex = startIndex
        self.endIndex = endIndex
        self.nextIndex = startIndex
        self.lineStartIndex = startIndex
    }
    
    public convenience init(string: String) {
        self.init(string: string, startIndex: string.startIndex, endIndex: string.endIndex)
    }
}

extension CharacterStream {
    public var isAtStartOfStream: Bool { return nextIndex == startIndex }
    public var isAtEndOfStream: Bool { return nextIndex == endIndex }
    public var columnNumber: Int { return columnOffset + 1 }
    fileprivate var columnOffset: Int { return string.distance(from: lineStartIndex, to: nextIndex) }
}

extension CharacterStream {
    fileprivate func advancingNextIndex(by n: Int) -> Index? {
        return advancingIndex(nextIndex, by: n)
    }
    
    fileprivate func advancingIndex(_ index: Index, by n: Int) -> Index? {
//        return string.index(index, offsetBy: n, limitedBy: (n < 0 ? startIndex : endIndex))
        if n == Int.min { return nil }
        var index = index
        let advance = n > 0 ? { index = self.string.index(after: index) } : { index = self.string.index(before: index) }
        let limit = n > 0 ? endIndex : startIndex
        for _ in 0..<abs(n) {
            if index == limit { return nil }
            advance()
        }
        return index
    }
}

extension CharacterStream {
    public func seek(to index: Index) {
        precondition(index >= startIndex, "index is less than startIndex")
        let newNextIndex = index < endIndex ? index : endIndex
        setNextIndex(newNextIndex)
    }
    
    public func seek(to indexToken: IndexToken) {
        let newNextIndex = indexToken.index(in: self)
        setNextIndex(newNextIndex)
    }
    
    fileprivate func setNextIndex(_ newNextIndex: Index) {
        if newNextIndex != nextIndex {
            nextIndex = newNextIndex
            stateTag += 1
        }
    }
}

extension CharacterStream {
    public func peek() -> Character? {
        return isAtEndOfStream ? nil : string[nextIndex]
    }
    
    public func peek(offset: Int) -> Character? {
        if let index = advancingNextIndex(by: offset), index != endIndex {
            return string[index]
        }
        return nil
    }
    
    public func peek(count: Int) -> String {
        precondition(count >= 0, "count is negative")
        let upperIndex = string.index(nextIndex, offsetBy: count, limitedBy: endIndex)
        return string[nextIndex..<(upperIndex ?? endIndex)]
    }
}

extension CharacterStream {
    public func match(_ c: Character) -> Bool {
        return !isAtEndOfStream && string[nextIndex] == c
    }
    
    public func match(_ predicate: (Character) -> Bool) -> Bool {
        return !isAtEndOfStream && predicate(string[nextIndex])
    }
    
    public func match(_ str: String, caseInsensitive: Bool = false) -> Bool {
        return match(str, from: nextIndex, caseInsensitive: caseInsensitive)
    }
    
    fileprivate func match(_ str: String, from start: Index, caseInsensitive: Bool) -> Bool {
        return indexAfterMatch(str, from: start, caseInsensitive: caseInsensitive) != nil
    }
    
    fileprivate func indexAfterMatch(_ str: String, from start: Index, caseInsensitive: Bool) -> Index? {
        precondition(start >= startIndex, "index is less than startIndex")
        if !caseInsensitive {
            var index = start
            for c in str.characters {
                if index == endIndex || c != string[index] {
                    return nil
                }
                index = string.index(after: index)
            }
            return index
        }
        
        guard let end = advancingIndex(start, by: str.characters.count) else {
            return nil
        }
        let options = String.CompareOptions.caseInsensitive
        guard string.compare(str, options: options, range: start..<end) == .orderedSame else {
            return nil
        }
        return end
    }
    
    public func match(_ regex: NSRegularExpression) -> NSTextCheckingResult? {
        func utf16IntRange(from: Index, to: Index, inString str: String) -> Range<Int> {
            let utf16View = str.utf16
            let utf16From = from.samePosition(in: utf16View)
            let utf16To = to.samePosition(in: utf16View)
            let location = utf16View.distance(from: utf16View.startIndex, to: utf16From)
            let count = utf16View.distance(from: utf16From, to: utf16To)
            return location..<(location+count)
        }
        
        let range = NSRange(utf16IntRange(from: nextIndex, to: endIndex, inString: string))
        return regex.firstMatch(in: string, options: [], range: range)
    }
}

extension CharacterStream {
    public func registerNewline() {
        assert(nextIndex != lineStartIndex)
        lineStartIndex = nextIndex
        lineNumber += 1
        stateTag += 1
    }
    
    public func registerNewlines(lineCount: Int, columnOffset: Int) {
        assert(lineCount != 0 && columnOffset >= 0)
        let newLineStartIndex = string.index(nextIndex, offsetBy: -columnOffset)
        assert(newLineStartIndex != lineStartIndex)
        lineStartIndex = newLineStartIndex
        lineNumber += lineCount
        assert(lineNumber > 0)
        stateTag += 1
    }
    
    fileprivate func registerNewlines(lineCount: Int, lineStartIndex: Index) {
        assert(lineCount > 0 && lineStartIndex > self.lineStartIndex)
        assert(lineStartIndex <= nextIndex)
        assert(lineStartIndex >= startIndex && lineStartIndex <= endIndex)
        self.lineStartIndex = lineStartIndex
        self.lineNumber += lineCount
        stateTag += 1
    }
}

extension CharacterStream {
    public func skip() {
        if isAtEndOfStream { return }
        nextIndex = string.index(after: nextIndex)
        stateTag += 1
    }
    
    public func skip(_ offset: Int) {
        let newIndex: Index
        if let advancedIndex = advancingNextIndex(by: offset) {
            newIndex = advancedIndex
        } else {
            precondition(offset > 0, "nextIndex + offset is less than startIndex")
            newIndex = endIndex
        }
        
        if nextIndex != newIndex {
            nextIndex = newIndex
            stateTag += 1
        }
    }
    
    @discardableResult
    public func skip(_ c: Character) -> Bool {
        guard match(c) else { return false }
        skip()
        return true
    }
    
    @discardableResult
    public func skip(_ predicate: (Character) -> Bool) -> Bool {
        guard match(predicate) else { return false }
        skip()
        return true
    }
    
    @discardableResult
    public func skip(_ str: String, caseInsensitive: Bool = false) -> Bool {
        guard let newNextIndex = indexAfterMatch(str, from: nextIndex,
                                                 caseInsensitive: caseInsensitive) else
        {
            return false
        }
        setNextIndex(newNextIndex)
        return true
    }
    
    public func skipAndPeek() -> Character? {
        skip()
        return peek()
    }
    
    public func skipAndPeek(offset: Int) -> Character? {
        let newIndex: Index
        let result: () -> Character?
        if let advancedIndex = advancingNextIndex(by: offset) {
            newIndex = advancedIndex
            result = peek
        } else {
            newIndex = offset > 0 ? endIndex : startIndex
            result = { nil }
        }
        
        if nextIndex != newIndex {
            nextIndex = newIndex
            stateTag += 1
        }
        return result()
    }
}

extension CharacterStream {
    public func read() -> Character? {
        if isAtEndOfStream { return nil }
        let result = string[nextIndex]
        nextIndex = string.index(after: nextIndex)
        stateTag += 1
        return result
    }
    
    public func read(_ count: Int) -> String {
        precondition(count >= 0, "count is negative")
        let newNextIndex = string.index(nextIndex, offsetBy: count, limitedBy: endIndex) ?? endIndex
        let result = string[nextIndex..<newNextIndex]
        setNextIndex(newNextIndex)
        return result
    }
    
    public func read(from indexToken: IndexToken) -> String {
        return read(from: indexToken.index(in: self))
    }
    
    internal func read(from index: Index) -> String {
        precondition(index <= nextIndex, "index is more than nextIndex")
        precondition(index >= startIndex, "index is less than startIndex")
        return string[index..<nextIndex]
    }
}

extension CharacterStream {
    public typealias Bounds = (range: Range<Index>, count: Int)
    
    @discardableResult
    public func skipWhitespaces() -> Bool {
        let count = skipSubstring(while: { Text.isWhitespaceChar($0) }).count
        return count != 0
    }
    
    @discardableResult
    public func skipNewline() -> Bool {
        guard let c = peek() else { return false }
        if !Text.isNewlineChar(c) { return false }
        nextIndex = string.index(after: nextIndex)
        registerNewline()
        return true
    }
    
    @discardableResult
    public func skipNewlineThenWhitespaces(tabWidth: Int) -> Int {
        precondition(tabWidth > 0, "tabWidth is negative")
        var indentWidth = -1
        skipSubstring(
            first: {
                if !Text.isNewlineChar($0) { return false }
                indentWidth = 0
                return true
            }, while: { c in
                if Text.isBlankChar(c) {
                    if c == "\t" {
                        indentWidth += tabWidth - (indentWidth % tabWidth)
                    } else {
                        indentWidth += 1
                    }
                    return true
                } else if Text.isNewlineChar(c) {
                    indentWidth = 0
                    return true
                } else {
                    return false
                }
            }
        )
        return indentWidth
    }
    
    @discardableResult
    public func skipRestOfLine(thenSkippingNewline skippingNewline: Bool = true) -> Bounds {
        if !skippingNewline {
            return skipSubstring {
                return !Text.isNewlineChar($0)
            }
        } else {
            var skippedNewline = false
            var result = skipSubstring {
                if skippedNewline { return false }
                skippedNewline = Text.isNewlineChar($0)
                return true
            }
            if skippedNewline {
                result.range = result.range.lowerBound..<advancingNextIndex(by: -1)!
            }
            return result
        }
    }
    
    @discardableResult
    public func skipSubstring(minCount: Int = 0, maxCount: Int = .max) -> Bounds
    {
        return skipSubstring(minCount: minCount, maxCount: maxCount, while: { _ in true })
    }
    
    @discardableResult
    public func skipSubstring(minCount: Int = 0, maxCount: Int = .max,
        while predicate: (Character) throws -> Bool)
        rethrows -> Bounds
    {
        return try skipSubstring(minCount: minCount, maxCount: maxCount,
                                 first: predicate, while: predicate)
    }
    
    @discardableResult
    public func skipSubstring(minCount: Int = 0, maxCount: Int = .max,
        first firstPredicate: (Character) throws -> Bool,
        while predicate: (Character) throws -> Bool)
        rethrows -> Bounds
    {
        precondition(maxCount >= 0, "maxCount is negative")
        var count: Int = 0
        var lineCount = 0
        var lineEndIndex: Index!
        var newNextIndex = nextIndex
        
        func advance(_ _predicate: (Character) throws -> Bool) rethrows -> Bool {
            if count == maxCount { return false }
            let c = string[newNextIndex]
            if try !_predicate(c) { return false }
            if Text.isNewlineChar(c) {
                lineCount += 1
                lineEndIndex = newNextIndex
            }
            count += 1
            newNextIndex = string.index(after: newNextIndex)
            return true
        }
        
        if try newNextIndex != endIndex && advance(firstPredicate) {
            while newNextIndex != endIndex {
                if try !advance(predicate) { break }
            }
        }
        
        if count < minCount { return (nextIndex..<nextIndex, 0) }
        
        let result = (nextIndex..<newNextIndex, count)
        
        if newNextIndex != nextIndex {
            nextIndex = newNextIndex
            if lineCount == 0 {
                stateTag += 1
            } else {
                registerNewlines(lineCount: lineCount, lineStartIndex: string.index(after: lineEndIndex))
            }
        }
        return result
    }
    
    @discardableResult
    public func skipSubstring(until
        str: String, maxCount: Int = .max, caseInsensitive: Bool = false)
        -> (found: Bool, bounds: Bounds)
    {
        precondition(!str.isEmpty, "string argument is empty")
        let firstChar = str[str.startIndex]
        var index = nextIndex
        let canMatch: (Character) -> Bool
        if let limit = advancingIndex(endIndex, by: -str.characters.count), index <= limit {
            canMatch = { c in c == firstChar && index <= limit }
        } else {
            canMatch = { _ in false }
        }
        var found = false
        
        let bounds = skipSubstring(maxCount: maxCount) { c in
            if canMatch(c) && match(str, from: index, caseInsensitive: caseInsensitive) {
                found = true
                return false
            }
            index = string.index(after: index)
            return true
        }
        
        if !found {
            found = match(str, from: index, caseInsensitive: caseInsensitive)
        }
        return (found, bounds)
    }
}

extension CharacterStream {
    public func readCharacter(normalizingNewline: Bool = true) -> Character? {
        if isAtEndOfStream { return nil }
        let c = string[nextIndex]
        nextIndex = string.index(after: nextIndex)
        if Text.isNewlineChar(c) {
            registerNewline()
            return normalizingNewline ? "\n" : c
        } else {
            stateTag += 1
            return c
        }
    }
    
    public func readRestOfLine(thenSkippingNewline skippingNewline: Bool = true) -> String {
        return string[skipRestOfLine(thenSkippingNewline: skippingNewline).range]
    }
    
    public func readSubstring(minCount: Int = 0, maxCount: Int = .max, normalizingNewlines: Bool = true)
        -> String
    {
        return readSubstring(minCount: minCount, maxCount: maxCount,
                             normalizingNewlines: normalizingNewlines, while: { _ in true })
    }
    
    public func readSubstring(minCount: Int = 0, maxCount: Int = .max, normalizingNewlines: Bool = true,
        while predicate: (Character) throws -> Bool)
        rethrows -> String
    {
        return try readSubstring(minCount: minCount, maxCount: maxCount,
                                 normalizingNewlines: normalizingNewlines,
                                 first: predicate, while: predicate)
    }
    
    public func readSubstring(minCount: Int = 0, maxCount: Int = .max, normalizingNewlines: Bool = true,
        first firstPredicate: (Character) throws -> Bool,
        while predicate: (Character) throws -> Bool)
        rethrows -> String
    {
        let oldLineNumber = lineNumber
        let (range, count) = try skipSubstring(minCount: minCount,
                                               maxCount: maxCount,
                                               first: firstPredicate,
                                               while: predicate)
        if !normalizingNewlines || oldLineNumber == lineNumber {
            return string[range]
        }
        return self.normalizingNewlines(in: range, countHint: count)
    }
    
    public func readSubstring(until
        str: String, maxCount: Int = .max,
        caseInsensitive: Bool = false, normalizingNewlines: Bool = true)
        -> (string: String?, bounds: Bounds)
    {
        let oldLineNumber = lineNumber
        let (found, bounds) = skipSubstring(until: str, maxCount: maxCount,
                                            caseInsensitive: caseInsensitive)
        if !found { return (nil, bounds) }
        if !normalizingNewlines || oldLineNumber == lineNumber {
            return (string[bounds.range], bounds)
        }
        return (self.normalizingNewlines(in: bounds.range, countHint: bounds.count), bounds)
    }
    
    fileprivate func normalizingNewlines(in range: Range<Index>, countHint: Int? = nil)
        -> String
    {
        var result = ""
        if let countHint = countHint {
            result.reserveCapacity(countHint)
        }
        for c in string.characters[range] {
            result.append(Text.isNewlineChar(c) ? "\n" : c)
        }
        return result
    }
}

// MARK: - IndexToken

extension CharacterStream {
    public struct IndexToken: Comparable {
        public let stream: CharacterStream
        public let index: Index
        
        internal init(stream: CharacterStream, index: Index) {
            self.stream = stream
            self.index = index
        }
        
        public func index(in stream: CharacterStream) -> Index {
            assert(stream === self.stream, "stream is different")
            return index
        }
        
        public static func == (lhs: IndexToken, rhs: IndexToken) -> Bool {
            return lhs.index == rhs.index
        }
        
        
        public static func < (lhs: IndexToken, rhs: IndexToken) -> Bool {
            return lhs.index < rhs.index
        }
    }
    
    public var indexToken: IndexToken {
        return IndexToken(stream: self, index: nextIndex)
    }
}

// MARK: - Position

extension CharacterStream {
    public struct Position: Comparable {
        public let name: String
        public let index: Index
        public let lineNumber: Int
        public let columnNumber: Int
        
        public static func == (lhs: Position, rhs: Position) -> Bool {
            return lhs.name == rhs.name
                && lhs.index == rhs.index
                && lhs.lineNumber == rhs.lineNumber
                && lhs.columnNumber == rhs.columnNumber
        }
        
        public static func < (lhs: Position, rhs: Position) -> Bool {
            if lhs.name != rhs.name { return lhs.name < rhs.name }
            if lhs.lineNumber != rhs.lineNumber { return lhs.lineNumber < rhs.lineNumber }
            if lhs.columnNumber != rhs.columnNumber { return lhs.columnNumber < rhs.columnNumber }
            return lhs.index < rhs.index
        }
    }
    
    public var position: Position {
        return Position(name: name, index: nextIndex, lineNumber: lineNumber,
                        columnNumber: columnNumber)
    }
}

// MARK: - State

extension CharacterStream {
    public struct State {
        public let stream: CharacterStream
        public let tag: Int
        public let index: CharacterStream.Index
        public let lineNumber: Int
        public let lineStartIndex: CharacterStream.Index
        public let name: String
        public let userInfo: CharacterStream.UserInfo
        
        fileprivate init(stream: CharacterStream) {
            self.stream = stream
            tag = stream.stateTag
            index = stream.nextIndex
            lineNumber = stream.lineNumber
            lineStartIndex = stream.lineStartIndex
            name = stream.name
            userInfo = stream.userInfo
        }
        
        public var indexToken: CharacterStream.IndexToken {
            return CharacterStream.IndexToken(stream: stream, index: index)
        }
        
        public func index(in stream: CharacterStream) -> CharacterStream.Index {
            assert(stream === self.stream, "stream is different")
            return index
        }
        
        public func position(in stream: CharacterStream) -> CharacterStream.Position {
            assert(stream === self.stream, "stream is different")
            let column = stream.string.distance(from: lineStartIndex, to: index) + 1
            return Position(name: name, index: index, lineNumber: lineNumber, columnNumber: column)
        }
    }
    
    public var state: State {
        return State(stream: self)
    }
    
    public func backtrack(to state: State) {
        assert(state.stream === self, "stream is different")
        assert(state.index >= startIndex && state.index <= endIndex)
        nextIndex = state.index
        lineNumber = state.lineNumber
        lineStartIndex = state.lineStartIndex
        name = state.name
        userInfo = state.userInfo
        stateTag = state.tag
    }
    
    public func read(from state: State, normalizingNewlines: Bool = true) -> String {
        let str = read(from: state.index(in: self))
        if !normalizingNewlines || state.lineNumber == lineNumber {
            return str
        }
        return Text.normalizingNewlines(of: str)
    }
    
    public func substream(from state: State) -> CharacterStream {
        assert(state.stream === self, "stream is different")
        precondition(state.index <= nextIndex, "state.index is more than nextIndex")
        let result = CharacterStream(string: string, startIndex: state.index, endIndex: nextIndex)
        result.lineNumber = state.lineNumber
        result.lineStartIndex = state.lineStartIndex
        result.name = state.name
        result.stateTag = 0
        return result
    }
}



