
// MARK: - Box

public final class Box<T> {
    public var value: T
    
    public init(_ value: T) {
        self.value = value
    }
    
    public func map<U>(f: (T) throws -> U) rethrows -> Box<U> {
        return Box<U>(try f(value))
    }
    
    public func flatMap<U>(f: (T) throws -> Box<U>) rethrows -> Box<U> {
        return try f(value)
    }
    
    public static func == <T: Equatable>(lhs: Box<T>, rhs: Box<T>) -> Bool {
        return lhs.value == rhs.value
    }
    
    public static func < <T: Comparable>(lhs: Box<T>, rhs: Box<T>) -> Bool {
        return lhs.value < rhs.value
    }
}

// MARK: - WeakBox

public final class WeakBox<T: AnyObject> {
    public weak var value: T?
    
    public init(_ value: T?) {
        self.value = value
    }
    
    public func map<U>(f: (T?) throws -> U?) rethrows -> WeakBox<U> {
        return WeakBox<U>(try f(value))
    }
    
    public func flatMap<U>(f: (T?) throws -> WeakBox<U>) rethrows -> WeakBox<U> {
        return try f(value)
    }
    
    public static func == <T: Equatable>(lhs: WeakBox<T>, rhs: WeakBox<T>) -> Bool {
        return lhs.value == rhs.value
    }
}



