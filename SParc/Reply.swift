
public enum Reply<T, Error: Swift.Error> {
    case success(T, [Error])
    case failure([Error])
    case fatalFailure([Error])
    
    public init(_ value: T) {
        self = .success(value, [])
    }
    
    public var value: T? {
        if case let .success(v, _) = self { return v }
        return nil
    }
    
    public var errors: [Error] {
        get {
            switch self {
            case let .success(_, e):    return e
            case let .failure(e):       return e
            case let .fatalFailure(e):  return e
            }
        }
        set {
            switch self {
            case .success(let v, _):    self = .success(v, newValue)
            case .failure(_):           self = .failure(newValue)
            case .fatalFailure(_):      self = .fatalFailure(newValue)
            }
        }
    }
    
    public func map<U>(f: (T) throws -> U) rethrows -> Reply<U, Error> {
        switch self {
        case let .success(v, e):    return try .success(f(v), e)
        case let .failure(e):       return .failure(e)
        case let .fatalFailure(e):  return .fatalFailure(e)
        }
    }
    
    public func flatMap<U>(f: (T) throws -> Reply<U, Error>) rethrows -> Reply<U, Error> {
        switch self {
        case let .success(v, e):    return try f(v).prependingErrors(e)
        case let .failure(e):       return .failure(e)
        case let .fatalFailure(e):  return .fatalFailure(e)
        }
    }
    
    public func prependingErrors(_ errors: [Error]) -> Reply {
        if errors.count == 0 { return self }
        switch self {
        case let .success(v, e):    return .success(v, errors + e)
        case let .failure(e):       return .failure(errors + e)
        case let .fatalFailure(e):  return .fatalFailure(errors + e)
        }
    }
    
    public func appendingErrors(_ errors: [Error]) -> Reply {
        if errors.count == 0 { return self }
        switch self {
        case let .success(v, e):    return .success(v, e + errors)
        case let .failure(e):       return .failure(e + errors)
        case let .fatalFailure(e):  return .fatalFailure(e + errors)
        }
    }
    
    public mutating func prependErrors(_ errors: [Error]) {
        self = prependingErrors(errors)
    }
    
    public mutating func appendErrors(_ errors: [Error]) {
        self = appendingErrors(errors)
    }
}



