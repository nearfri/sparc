
// MARK: - Running parsers on input

// TODO: 이게 최선인지 재고하기
public enum ParserResult<T> {
    case success(T, CharacterStream.UserInfo, CharacterStream.Position)
    case failure([ParserError], CharacterStream.UserInfo, CharacterStream.Position)
}

// TODO: run() 함수 제거
public func run<T>(parser: Parser<T>, string: String, streamName: String = "") -> ParserResult<T> {
    let stream = CharacterStream(string: string)
    stream.name = streamName
    
    let reply = parser.parse(stream)
    switch reply {
    case let .success(v, _):
        return .success(v, stream.userInfo, stream.position)
    default:
        return .failure(reply.errors, stream.userInfo, stream.position)
    }
}

extension Parser {
    public func run(_ string: String) -> ParserResult<Result> {
        let stream = CharacterStream(string: string)
        switch parse(stream) {
        case let .success(v, _):
            return .success(v, stream.userInfo, stream.position)
        case let .failure(e), let .fatalFailure(e):
            return .failure(e, stream.userInfo, stream.position)
        }
    }
}



