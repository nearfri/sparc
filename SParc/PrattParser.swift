
public enum TokenType: Int {
    case end
    case number
    case string
    case `operator`
    case name
    case comment
}

public struct Token: Hashable {
    public var type: TokenType
    public var value: String
    
    public init(type: TokenType, value: String = "") {
        self.type = type
        self.value = value
    }
    
    public static func == (lhs: Token, rhs: Token) -> Bool {
        return lhs.type == rhs.type && lhs.value == rhs.value
    }
    
    public var hashValue: Int {
        return type.hashValue ^ value.hashValue
    }
}

public protocol TokenHandling {
    
}

public struct NullDenotation<T>: TokenHandling {
    public typealias Expression = (_ token: Token, _ parser: PrattParser<T>) throws -> T
    
    public let expression: Expression
    
    public init(expression: @escaping Expression) {
        self.expression = expression
    }
}

public struct LeftDenotation<T>: TokenHandling {
    public typealias Expression =
        (_ leftValue: T, _ token: Token, _ parser: PrattParser<T>) throws -> T
    
    public let bindingPower: Int
    public let expression: Expression
    
    public init(bindingPower: Int, expression: @escaping Expression) {
        self.bindingPower = bindingPower
        self.expression = expression
    }
}

private class DenotationGroup<Denotation: TokenHandling> {
    var denotations: [String: Denotation] = [:]
    var commonDenotation: Denotation?
    
    subscript(token: Token) -> Denotation? {
        return denotations[token.value] ?? commonDenotation
    }
}

public final class PrattParser<T> {
    fileprivate var nullDenotationGroups: [TokenType: DenotationGroup<NullDenotation<T>>] = [:]
    fileprivate var leftDenotationGroups: [TokenType: DenotationGroup<LeftDenotation<T>>] = [:]
    
    public fileprivate(set) var nextToken: Token = Token(type: .end)
    public fileprivate(set) var advance: () throws -> Void = {}
    
    public init() {}
    
    public func parse<S: Sequence>(tokens: S) -> Reply<T, ParserError>
        where S.Iterator.Element == Token
    {
        let tokens = AnyIterator(tokens.makeIterator())
        advance = {
            if let token = tokens.next() {
                self.nextToken = token
            } else if self.nextToken.type != .end {
                self.nextToken = Token(type: .end)
            }
        }
        
        return parse()
    }
    
    public func parse(_ stream: CharacterStream, with tokenizer: Parser<Token>)
        -> Reply<T, ParserError>
    {
        advance = {
            switch tokenizer.parse(stream) {
            case let .success(token, _):
                self.nextToken = token
            case let .failure(e):
                throw PrattParserError.failure(e)
            case let .fatalFailure(e):
                throw PrattParserError.fatalFailure(e)
            }
        }
        
        return parse()
    }
    
    private func parse() -> Reply<T, ParserError> {
        defer {
            nextToken = Token(type: .end)
            advance = {}
        }
        
        do {
            try advance()
            let ret = try expression(withRightBindingPower: 0)
            return .success(ret, [])
        } catch PrattParserError.failure(let e) {
            return .failure(e)
        } catch PrattParserError.fatalFailure(let e) {
            return .fatalFailure(e)
        } catch let e as ParserError {
            return .fatalFailure([e])
        } catch let e {
            return .fatalFailure([.Other(message: "\(e)")])
        }
    }
    
    public func expression(withRightBindingPower rightBindingPower: Int) throws -> T {
        let token = nextToken
        let nud = try nullDenotation(for: token)
        try advance()
        var leftValue = try nud.expression(token, self)
        while true {
            let token = nextToken
            let led = try leftDenotation(for: token)
            guard rightBindingPower < led.bindingPower else {
                break
            }
            try advance()
            leftValue = try led.expression(leftValue, token, self)
        }
        return leftValue
    }
}

extension PrattParser {
    public func add(denotation: NullDenotation<T>, for token: Token) {
        let group = denotationGroup(for: token.type, in: &nullDenotationGroups)
        group.denotations[token.value] = denotation
    }
    
    public func add(denotation: NullDenotation<T>, for tokenType: TokenType) {
        let group = denotationGroup(for: tokenType, in: &nullDenotationGroups)
        group.commonDenotation = denotation
    }
    
    public func addDenotation(
        for token: Token, expression: @escaping NullDenotation<T>.Expression)
    {
        add(denotation: NullDenotation<T>(expression: expression), for: token)
    }
    
    public func addDenotation(
        for tokenType: TokenType, expression: @escaping NullDenotation<T>.Expression)
    {
        add(denotation: NullDenotation<T>(expression: expression), for: tokenType)
    }
    
    public func add(denotation: LeftDenotation<T>, for token: Token) {
        let group = denotationGroup(for: token.type, in: &leftDenotationGroups)
        group.denotations[token.value] = denotation
    }
    
    public func add(denotation: LeftDenotation<T>, for tokenType: TokenType) {
        let group = denotationGroup(for: tokenType, in: &leftDenotationGroups)
        group.commonDenotation = denotation
    }
    
    public func addDenotation(
        for token: Token, bindingPower: Int, expression: @escaping LeftDenotation<T>.Expression)
    {
        let denotation = LeftDenotation<T>(bindingPower: bindingPower, expression: expression)
        add(denotation: denotation, for: token)
    }
    
    public func addDenotation(
        for tokenType: TokenType, bindingPower: Int,
        expression: @escaping LeftDenotation<T>.Expression)
    {
        let denotation = LeftDenotation<T>(bindingPower: bindingPower, expression: expression)
        add(denotation: denotation, for: tokenType)
    }
}

extension PrattParser {
    fileprivate func nullDenotation(for token: Token) throws -> NullDenotation<T> {
        guard let result: NullDenotation = denotation(for: token, in: nullDenotationGroups) else {
            let message = "could not find the null denotation for \(token)"
            throw PrattParserError.fatalFailure([ParserError.Other(message: message)])
        }
        return result
    }
    
    fileprivate func leftDenotation(for token: Token) throws -> LeftDenotation<T> {
        guard let result: LeftDenotation = denotation(for: token, in: leftDenotationGroups) else {
            let message = "could not find the left denotation for \(token)"
            throw PrattParserError.fatalFailure([ParserError.Other(message: message)])
        }
        return result
    }
    
    fileprivate func denotation<Denotation>(
        for token: Token, in denotationGroups: [TokenType: DenotationGroup<Denotation>])
        -> Denotation?
    {
        guard let denotationGroup = denotationGroups[token.type] else {
            return nil
        }
        return denotationGroup[token]
    }
    
    fileprivate func denotationGroup<Denotation>(
        for type: TokenType, in denotationGroups: inout [TokenType: DenotationGroup<Denotation>])
        -> DenotationGroup<Denotation>
    {
        let result: DenotationGroup<Denotation>
        if let group = denotationGroups[type] {
            result = group
        } else {
            result = DenotationGroup<Denotation>()
            denotationGroups[type] = result
        }
        return result
    }
}

public enum PrattParserError: Error {
    case failure([ParserError])
    case fatalFailure([ParserError])
}



