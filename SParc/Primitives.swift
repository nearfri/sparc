
infix operator >>- : AdditionPrecedence
infix operator |>> : AdditionPrecedence
infix operator ^>> : AdditionPrecedence
infix operator >>% : AdditionPrecedence
infix operator >>! : AdditionPrecedence
infix operator !>> : AdditionPrecedence
infix operator !>>! : AdditionPrecedence
infix operator <|> : AdditionPrecedence
infix operator <|>% : AdditionPrecedence
infix operator >>-? : AdditionPrecedence
infix operator >>!? : AdditionPrecedence
infix operator !>>? : AdditionPrecedence
infix operator !>>!? : AdditionPrecedence
infix operator <?> : AdditionPrecedence
infix operator <??> : AdditionPrecedence

public struct Parser<Result> {
    public let parse: (CharacterStream) -> Reply<Result, ParserError>
    
    public init(_ parse: @escaping (CharacterStream) -> Reply<Result, ParserError>) {
        self.parse = parse
    }
}

public func unit<T>(_ reply: Reply<T, ParserError>) -> Parser<T> {
    return Parser { _ in reply }
}

public func unit<T>(_ x: T) -> Parser<T> {
    return Parser { _ in Reply(x) }
}

// MARK: - Chaining and piping parsers

extension Parser {
    public func flatMap<T>(_ transform: @escaping (Result) -> Parser<T>) -> Parser<T> {
        return self >>- transform
    }
    
    public func map<T>(_ transform: @escaping (Result) -> T) -> Parser<T> {
        return self |>> transform
    }
}

public func >>- <T1, T2>(p: Parser<T1>, f: @escaping (T1) -> Parser<T2>) -> Parser<T2> {
    return Parser { stream in
        switch p.parse(stream) {
        case let .failure(e):
            return .failure(e)
        case let .fatalFailure(e):
            return .fatalFailure(e)
        case let .success(v, e):
            let stateTag = stream.stateTag
            let reply = f(v).parse(stream)
            return stateTag == stream.stateTag && e.count > 0 ? reply.prependingErrors(e) : reply
        }
    }
}

public func |>> <T1, T2>(p: Parser<T1>, f: @escaping (T1) -> T2) -> Parser<T2> {
    return p >>- { a in unit(f(a)) }
}

public func ^>> <T1, T2>(p: Parser<T1>, f: @escaping (T1) -> Reply<T2, ParserError>) -> Parser<T2> {
    return p >>- { a in unit(f(a)) }
}

public func >>% <T1, T2>(p: Parser<T1>, x: T2) -> Parser<T2> {
     return p >>- { _ in unit(x) }
}

public func >>! <T1, T2>(p1: Parser<T1>, p2: Parser<T2>) -> Parser<T2> {
    return p1 >>- { _ in p2 }
}

public func !>> <T1, T2>(p1: Parser<T1>, p2: Parser<T2>) -> Parser<T1> {
    return p1 >>- { a in p2 >>% a }
}

public func !>>! <T1, T2>(p1: Parser<T1>, p2: Parser<T2>) -> Parser<(T1, T2)> {
    return p1 >>- { a in p2 >>- { b in unit((a, b)) } }
}

public func between<T1, T2, T3>(open: Parser<T1>, close: Parser<T2>, parser: Parser<T3>)
    -> Parser<T3>
{
    return open >>! parser !>> close
}

public func pipe<T1, T2, T3>(_ p1: Parser<T1>, _ p2: Parser<T2>, _ f: @escaping (T1, T2) -> T3)
    -> Parser<T3>
{
    return p1 >>- { a in p2 >>- { b in unit(f(a, b)) } }
}

public func pipe<T1, T2, T3, T4>(
    _ p1: Parser<T1>, _ p2: Parser<T2>, _ p3: Parser<T3>, _ f: @escaping (T1, T2, T3) -> T4)
    -> Parser<T4>
{
    return p1 >>- { a in p2 >>- { b in p3 >>- { c in unit(f(a, b, c)) } } }
}

public func pipe<T1, T2, T3, T4, T5>(
    _ p1: Parser<T1>, _ p2: Parser<T2>, _ p3: Parser<T3>, _ p4: Parser<T4>,
    _ f: @escaping (T1, T2, T3, T4) -> T5)
    -> Parser<T5>
{
    return p1 >>- { a in p2 >>- { b in p3 >>- { c in p4 >>- { d in unit(f(a, b, c, d)) } } } }
}

public func pipe<T1, T2, T3, T4, T5, T6>(
    _ p1: Parser<T1>, _ p2: Parser<T2>, _ p3: Parser<T3>, _ p4: Parser<T4>,
    _ p5: Parser<T5>, _ f: @escaping (T1, T2, T3, T4, T5) -> T6)
    -> Parser<T6>
{
    return p1 >>- { a in p2 >>- { b in p3 >>- { c in p4 >>- { d in p5 >>- { e in
        unit(f(a, b, c, d, e)) } } } } }
}

// MARK: - Parsing alternatives and recovering from errors

public func <|> <T>(p1: Parser<T>, p2: Parser<T>) -> Parser<T> {
    return Parser { stream in
        let stateTag = stream.stateTag
        let reply = p1.parse(stream)
        guard stateTag == stream.stateTag, case let .failure(e) = reply else {
            return reply
        }
        let reply2 = p2.parse(stream)
        return stateTag == stream.stateTag && e.count > 0 ? reply2.prependingErrors(e) : reply2
    }
}

#if false
public func <|>% <T>(p: Parser<T>, x: T) -> Parser<T> {
    return p <|> unit(x)
}
#endif

public func choice<T, S: Sequence>(_ ps: S) -> Parser<T> where S.Iterator.Element == Parser<T> {
    return Parser { stream in
        let stateTag = stream.stateTag
        var reply: Reply<T, ParserError> = .failure([])
        var errors: [ParserError] = []
        for p in ps {
            guard stateTag == stream.stateTag, case let .failure(e) = reply else { break }
            errors += e
            reply = p.parse(stream)
        }
        if stateTag == stream.stateTag {
            reply.errors = errors + reply.errors
        }
        return reply
    }
}

public func optional<T>(_ p: Parser<T>) -> Parser<T?> {
    #if true
    return p |>> Optional.init <|> unit(nil)
    #else
    return Parser { stream in
        let stateTag = stream.stateTag
        switch p.parse(stream) {
        case let .success(v, e):
            return .success(v, e)
        case let .failure(e) where stateTag == stream.stateTag:
            return .success(nil, e)
        case let .failure(e):
            return .failure(e)
        case let .fatalFailure(e):
            return .fatalFailure(e)
        }
    }
    #endif
}

public func skip<T>(_ p: Parser<T>) -> Parser<Void> {
    #if true
        return p >>% () <|> unit(())
    #else
        return Parser { stream in
            let stateTag = stream.stateTag
            switch p.parse(stream) {
            case let .success(_, e):
                return .success((), e)
            case let .failure(e) where stateTag == stream.stateTag:
                return .success((), e)
            case let .failure(e):
                return .failure(e)
            case let .fatalFailure(e):
                return .fatalFailure(e)
            }
        }
    #endif
}

public func attempt<T>(_ p: Parser<T>) -> Parser<T> {
    return Parser { stream in
        let state = stream.state
        let reply = p.parse(stream)
        if case .success = reply { return reply }
        
        if state.tag == stream.stateTag {
            return .failure(reply.errors)
        }
        defer { stream.backtrack(to: state) }
        if reply.errors.count == 1 && reply.errors[0] is NestedParserError {
            return .failure(reply.errors)
        }
        return .failure([NestedParserError(position: stream.position,
                                           userInfo: stream.userInfo,
                                           errors: reply.errors)])
    }
}

public func >>-? <T1, T2>(p: Parser<T1>, f: @escaping (T1) -> Parser<T2>) -> Parser<T2> {
    return Parser { stream in
        let state = stream.state
        switch p.parse(stream) {
        case let .failure(e):
            return .failure(e)
        case let .fatalFailure(e):
            return .fatalFailure(e)
        case let .success(v, e):
            let stateTag1 = stream.stateTag
            let reply2 = f(v).parse(stream)
            if stateTag1 != stream.stateTag { return reply2 }
            switch reply2 {
            case .success, .fatalFailure:
                return reply2.prependingErrors(e)
            case .failure where stateTag1 == state.tag:
                return reply2.prependingErrors(e)
            case let .failure(e2):
                defer { stream.backtrack(to: state) }
                let errors = e + e2
                if errors.count == 1 && errors[0] is NestedParserError {
                    return .failure(errors)
                }
                return .failure([NestedParserError(position: stream.position,
                                                   userInfo: stream.userInfo,
                                                   errors: errors)])
            }
        }
    }
}

public func >>!? <T1, T2>(p1: Parser<T1>, p2: Parser<T2>) -> Parser<T2> {
    return p1 >>-? { _ in p2 }
}

public func !>>? <T1, T2>(p1: Parser<T1>, p2: Parser<T2>) -> Parser<T1> {
    return p1 >>-? { a in p2 >>% a }
}

public func !>>!? <T1, T2>(p1: Parser<T1>, p2: Parser<T2>) -> Parser<(T1, T2)> {
    return p1 >>-? { a in p2 >>- { b in unit((a, b)) } }
}

// MARK: - Conditional parsing and looking ahead

public func notEmpty<T>(_ p: Parser<T>) -> Parser<T> {
    return Parser { stream in
        let stateTag = stream.stateTag
        let reply = p.parse(stream)
        if stateTag == stream.stateTag, case let .success(_, e) = reply {
            return .failure(e)
        }
        return reply
    }
}

public func followed<T>(by p: Parser<T>, errorLabel: String? = nil) -> Parser<Void> {
    return Parser { stream in
        let state = stream.state
        let reply = p.parse(stream)
        if state.tag != stream.stateTag {
            stream.backtrack(to: state)
        }
        if case .success = reply {
            return Reply(())
        }
        return .failure(errorLabel.map({ [ParserError.Expected($0)] }) ?? [])
    }
}

public func notFollowed<T>(by p: Parser<T>, errorLabel: String? = nil) -> Parser<Void> {
    return Parser { stream in
        let state = stream.state
        let reply = p.parse(stream)
        if state.tag != stream.stateTag {
            stream.backtrack(to: state)
        }
        if case .success = reply {
            return .failure(errorLabel.map({ [ParserError.Unexpected($0)] }) ?? [])
        }
        return Reply(())
    }
}

public func lookAhead<T>(_ p: Parser<T>) -> Parser<T> {
    return Parser { stream in
        let state = stream.state
        let reply = p.parse(stream)
        if case let .success(v, _) = reply {
            if state.tag != stream.stateTag {
                stream.backtrack(to: state)
            }
            return .success(v, [])
        }
        
        if state.tag == stream.stateTag {
            return .failure(reply.errors)
        }
        defer { stream.backtrack(to: state) }
        if reply.errors.count == 1 && reply.errors[0] is NestedParserError {
            return .failure(reply.errors)
        }
        return .failure([NestedParserError(position: stream.position,
                                           userInfo: stream.userInfo,
                                           errors: reply.errors)])
    }
}

// MARK: - Customizing error messages

public func <?> <T>(p: Parser<T>, errorLabel: String) -> Parser<T> {
    return Parser { stream in
        let stateTag = stream.stateTag
        var reply = p.parse(stream)
        if stateTag == stream.stateTag {
            reply.errors = [ParserError.Expected(errorLabel)]
        }
        return reply
    }
}

public func <??> <T>(p: Parser<T>, errorLabel: String) -> Parser<T> {
    let expectedError = [ParserError.Expected(errorLabel)]
    return Parser { stream in
        let state = stream.state
        var reply = p.parse(stream)
        if case let .success(v, e) = reply {
            return .success(v, state.tag != stream.stateTag ? e : expectedError)
        }
        
        if state.tag != stream.stateTag {
            defer { stream.backtrack(to: state) }
            typealias Stream = CharacterStream
            let (position, userInfo, errors): (Stream.Position, Stream.UserInfo, [ParserError])
            if reply.errors.count == 1, let nestedError = reply.errors[0] as? NestedParserError {
                (position, userInfo, errors) = (nestedError.position, nestedError.userInfo,
                                                nestedError.errors)
            } else {
                (position, userInfo, errors) = (stream.position, stream.userInfo, reply.errors)
            }
            return .fatalFailure([CompoundParserError(position: position, userInfo: userInfo,
                                                      errors: errors, label: errorLabel)])
        } else {
            if reply.errors.count == 1, let nestedError = reply.errors[0] as? NestedParserError {
                reply.errors = [CompoundParserError(
                    position: nestedError.position, userInfo: nestedError.userInfo,
                    errors: nestedError.errors, label: errorLabel)]
            } else {
                reply.errors = expectedError
            }
            return reply
        }
    }
}

public func fail<T>(_ message: String) -> Parser<T> {
    return unit(.failure([.Other(message: message)]))
}

public func failFatally<T>(_ message: String) -> Parser<T> {
    return unit(.fatalFailure([.Other(message: message)]))
}

// MARK: - Value aggregators

internal protocol Aggregator {
    associatedtype Value
    associatedtype Result
    
    var result: Result { get }
    mutating func valueOccurred(_ value: Value)
}

private struct ValueAggregator<Value>: Aggregator {
    var result: [Value] = []
    mutating func valueOccurred(_ value: Value) {
        result.append(value)
    }
}

private struct VoidAggregator<Value>: Aggregator {
    var result: Void { return () }
    mutating func valueOccurred(_ value: Value) {}
}

// MARK: - Parsing sequences

public func tuple<T1, T2>(_ p1: Parser<T1>, _ p2: Parser<T2>) -> Parser<(T1, T2)> {
    return pipe(p1, p2) { ($0, $1) }
}

public func tuple<T1, T2, T3>(_ p1: Parser<T1>, _ p2: Parser<T2>, _ p3: Parser<T3>)
    -> Parser<(T1, T2, T3)>
{
    return pipe(p1, p2, p3) { ($0, $1, $2) }
}

public func tuple<T1, T2, T3, T4>(
    _ p1: Parser<T1>, _ p2: Parser<T2>, _ p3: Parser<T3>, _ p4: Parser<T4>)
    -> Parser<(T1, T2, T3, T4)>
{
    return pipe(p1, p2, p3, p4) { ($0, $1, $2, $3) }
}

public func tuple<T1, T2, T3, T4, T5>(
    _ p1: Parser<T1>, _ p2: Parser<T2>, _ p3: Parser<T3>, _ p4: Parser<T4>,
    _ p5: Parser<T5>)
    -> Parser<(T1, T2, T3, T4, T5)>
{
    return pipe(p1, p2, p3, p4, p5) { ($0, $1, $2, $3, $4) }
}

public func array<T>(_ parser: Parser<T>, count: Int) -> Parser<[T]> {
    return array(parser, count: count, aggregatorGenerator: ValueAggregator.init)
}

public func skipArray<T>(_ parser: Parser<T>, count: Int) -> Parser<Void> {
    return array(parser, count: count, aggregatorGenerator: VoidAggregator.init)
}

private func array<T, A: Aggregator>(
    _ parser: Parser<T>, count: Int, aggregatorGenerator: @escaping () -> A)
    -> Parser<A.Result> where A.Value == T
{
    return Parser { stream in
        var lastReply: Reply<T, ParserError>? = nil
        var aggregator = aggregatorGenerator()
        var errors: [ParserError] = []
        for _ in 0..<count {
            let stateTag = stream.stateTag
            let reply = parser.parse(stream)
            lastReply = reply
            errors = stateTag != stream.stateTag ? reply.errors : errors + reply.errors
            if case let .success(v, _) = reply {
                aggregator.valueOccurred(v)
            } else {
                break
            }
        }
        switch lastReply {
        case nil, .success?:
            return .success(aggregator.result, errors)
        case .failure?:
            return .failure(errors)
        case .fatalFailure?:
            return .fatalFailure(errors)
        }
    }
}

public func many<T>(_ p: Parser<T>, atLeastOnce: Bool = false) -> Parser<[T]> {
    return many(first: p, while: p, atLeastOnce: atLeastOnce,
                aggregatorGenerator: ValueAggregator.init)
}

public func many<T>(
    first firstParser: Parser<T>, while parser: Parser<T>, atLeastOnce: Bool = false)
    -> Parser<[T]>
{
    return many(first: firstParser, while: parser, atLeastOnce: atLeastOnce,
                aggregatorGenerator: ValueAggregator.init)
}

public func skipMany<T>(_ p: Parser<T>, atLeastOnce: Bool = false) -> Parser<Void> {
    return many(first: p, while: p, atLeastOnce: atLeastOnce,
                aggregatorGenerator: VoidAggregator.init)
}

public func skipMany<T>(
    first firstParser: Parser<T>, while parser: Parser<T>, atLeastOnce: Bool = false)
    -> Parser<Void>
{
    return many(first: firstParser, while: parser, atLeastOnce: atLeastOnce,
                aggregatorGenerator: VoidAggregator.init)
}

internal func many<T, A: Aggregator>(first
    firstParser: Parser<T>, while parser: Parser<T>, atLeastOnce: Bool,
    function: String = #function, aggregatorGenerator: @escaping () -> A)
    -> Parser<A.Result> where A.Value == T
{
    return Parser { stream in
        var aggregator = aggregatorGenerator()
        var errors: [ParserError]
        let firstStateTag = stream.stateTag
        switch firstParser.parse(stream) {
        case let .success(v, e):
            aggregator.valueOccurred(v)
            errors = e
        case let .failure(e) where firstStateTag == stream.stateTag && !atLeastOnce:
            return .success(aggregator.result, e)
        case let .failure(e):
            return .failure(e)
        case let .fatalFailure(e):
            return .fatalFailure(e)
        }
        
        repeat {
            let stateTag = stream.stateTag
            switch parser.parse(stream) {
            case let .success(v, e):
                precondition(stateTag != stream.stateTag, infiniteLoopErrorMessage(function))
                aggregator.valueOccurred(v)
                errors = e
            case let .failure(e) where stateTag == stream.stateTag:
                return .success(aggregator.result, errors + e)
            case let .failure(e):
                return .failure(e)
            case let .fatalFailure(e):
                return .fatalFailure(stateTag != stream.stateTag ? e : errors + e)
            }
        } while true
    }
}

private func infiniteLoopErrorMessage(_ function: String) -> String {
    return "The combinator '\(function)' was applied to a parser that succeeds "
        + "without consuming input and without changing the parser state in any other way. "
        + "(If no exception had been raised, the combinator likely would have "
        + "entered an infinite loop.)"
}

public func many<T1, T2>(
    _ p: Parser<T1>, separator: Parser<T2>,
    atLeastOnce: Bool = false, allowEndBySeparator: Bool = false)
    -> Parser<[T1]>
{
    return many(p, separator: separator, atLeastOnce: atLeastOnce,
                allowEndBySeparator: allowEndBySeparator,
                aggregatorGenerator: ValueSeparatorAggregator.init)
}

public func skipMany<T1, T2>(
    _ p: Parser<T1>, separator: Parser<T2>,
    atLeastOnce: Bool = false, allowEndBySeparator: Bool = false)
    -> Parser<Void>
{
    return many(p, separator: separator, atLeastOnce: atLeastOnce,
                allowEndBySeparator: allowEndBySeparator,
                aggregatorGenerator: VoidSeparatorAggregator.init)
}

internal protocol SeparatorHandling {
    associatedtype Separator
    
    mutating func separatorOccurred(_ separator: Separator)
}

private struct ValueSeparatorAggregator<Value, Separator>: Aggregator, SeparatorHandling {
    var result: [Value] = []
    mutating func valueOccurred(_ value: Value) {
        result.append(value)
    }
    mutating func separatorOccurred(_ separator: Separator) {}
}

private struct VoidSeparatorAggregator<Value, Separator>: Aggregator, SeparatorHandling {
    var result: Void { return () }
    mutating func valueOccurred(_ value: Value) {}
    mutating func separatorOccurred(_ separator: Separator) {}
}

internal func many<T1, T2, A: Aggregator>(
    _ p: Parser<T1>, separator: Parser<T2>, atLeastOnce: Bool,
    allowEndBySeparator: Bool, function: String = #function, aggregatorGenerator: @escaping () -> A)
    -> Parser<A.Result>
    where A: SeparatorHandling, A.Value == T1, A.Separator == T2
{
    return Parser { stream in
        var aggregator = aggregatorGenerator()
        var errors: [ParserError] = []
        var satisfiesAtLeastCondition = !atLeastOnce
        repeat {
            let stateTag = stream.stateTag
            switch p.parse(stream) {
            case let .success(v, e):
                aggregator.valueOccurred(v)
                errors = stateTag != stream.stateTag ? e : errors + e
                satisfiesAtLeastCondition = true
            case let .failure(e) where allowEndBySeparator:
                if stateTag != stream.stateTag || !satisfiesAtLeastCondition {
                    return .failure(e)
                }
                return .success(aggregator.result, errors + e)
            case let .failure(e):
                return .failure(stateTag != stream.stateTag ? e : errors + e)
            case let .fatalFailure(e):
                return .fatalFailure(stateTag != stream.stateTag ? e : errors + e)
            }
            
            let sepStateTag = stream.stateTag
            switch separator.parse(stream) {
            case let .success(v, e):
                precondition(stateTag != stream.stateTag, infiniteLoopErrorMessage(function))
                aggregator.separatorOccurred(v)
                errors = sepStateTag != stream.stateTag ? e : errors + e
            case let .failure(e) where sepStateTag != stream.stateTag:
                return .failure(e)
            case let .failure(e):
                return .success(aggregator.result, errors + e)
            case let .fatalFailure(e):
                return .fatalFailure(sepStateTag != stream.stateTag ? e : errors + e)
            }
        } while true
    }
}

public func many<T1, T2>(_ p: Parser<T1>, until endParser: Parser<T2>, atLeastOnce: Bool = false)
    -> Parser<[T1]>
{
    return many(first: p, while: p, until: endParser, atLeastOnce: atLeastOnce, apply: { $0.0 },
                aggregatorGenerator: ValueAggregator.init)
}

public func skipMany<T1, T2>(
    _ p: Parser<T1>, until endParser: Parser<T2>, atLeastOnce: Bool = false)
    -> Parser<Void>
{
    return many(first: p, while: p, until: endParser, atLeastOnce: atLeastOnce, apply: { $0.0 },
                aggregatorGenerator: VoidAggregator.init)
}

internal func many<T1, T2, T3, A: Aggregator>(first
    firstParser: Parser<T1>, while parser: Parser<T1>,
    until endParser: Parser<T2>, atLeastOnce: Bool,
    apply transform: @escaping (A.Result, T2) -> T3,
    function: String = #function, aggregatorGenerator: @escaping () -> A)
    -> Parser<T3> where A.Value == T1
{
    return Parser { stream in
        var aggregator = aggregatorGenerator()
        var errors: [ParserError] = []
        if !atLeastOnce {
            let endStateTag = stream.stateTag
            switch endParser.parse(stream) {
            case let .success(v, e):
                return .success(transform(aggregator.result, v), e)
            case let .failure(e) where endStateTag != stream.stateTag:
                return .failure(e)
            case let .failure(e):
                errors = e
            case let .fatalFailure(e):
                return .fatalFailure(e)
            }
        }
        
        let stateTag = stream.stateTag
        switch firstParser.parse(stream) {
        case let .success(v, e):
            aggregator.valueOccurred(v)
            errors = stateTag != stream.stateTag ? e : errors + e
        case let .failure(e):
            return .failure(stateTag != stream.stateTag ? e : errors + e)
        case let .fatalFailure(e):
            return .fatalFailure(stateTag != stream.stateTag ? e : errors + e)
        }
        
        return many(parser, until: endParser, apply: transform, stream: stream,
            aggregator: aggregator, errors: errors, function: function)
    }
}

private func many<T1, T2, T3, A: Aggregator>(
    _ parser: Parser<T1>, until endParser: Parser<T2>, apply transform: (A.Result, T2) -> T3,
    stream: CharacterStream, aggregator: A, errors: [ParserError], function: String)
    -> Reply<T3, ParserError> where A.Value == T1
{
    var aggregator = aggregator
    var errors = errors
    repeat {
        let endStateTag = stream.stateTag
        switch endParser.parse(stream) {
        case let .success(v, e):
            return .success(transform(aggregator.result, v),
                            endStateTag != stream.stateTag ? e : errors + e)
        case let .failure(e) where endStateTag != stream.stateTag:
            return .failure(e)
        case let .failure(e):
            errors += e
        case let .fatalFailure(e):
            return .fatalFailure(endStateTag != stream.stateTag ? e : errors + e)
        }
        
        let stateTag = stream.stateTag
        switch parser.parse(stream) {
        case let .success(v, e):
            precondition(stateTag != stream.stateTag, infiniteLoopErrorMessage(function))
            aggregator.valueOccurred(v)
            errors = e
        case let .failure(e):
            return .failure(stateTag != stream.stateTag ? e : errors + e)
        case let .fatalFailure(e):
            return .fatalFailure(stateTag != stream.stateTag ? e : errors + e)
        }
    } while true
}

public func chainLeft<T>(_ p: Parser<T>, combiner: Parser<(T, T) -> T>) -> Parser<T> {
    return many(p, separator: combiner, atLeastOnce: true,
                allowEndBySeparator: false, aggregatorGenerator: ValueReducer.init)
}

public func chainLeft<T>(_ p: Parser<T>, combiner: Parser<(T, T) -> T>, otherwise: T) -> Parser<T> {
    return chainLeft(p, combiner: combiner) <|> unit(otherwise)
}

public func chainRight<T>(_ p: Parser<T>, combiner: Parser<(T, T) -> T>) -> Parser<T> {
    return many(p, separator: combiner, atLeastOnce: true,
                allowEndBySeparator: false, aggregatorGenerator: ValueReverseReducer.init)
}

public func chainRight<T>(_ p: Parser<T>, combiner: Parser<(T, T) -> T>, otherwise: T)
    -> Parser<T>
{
    return chainRight(p, combiner: combiner) <|> unit(otherwise)
}

private struct ValueReducer<Value>: Aggregator, SeparatorHandling {
    typealias Combine = (Value, Value) -> Value
    
    var accumulation: Value? = nil
    var combine: Combine = { _, v2 in v2 }
    var result: Value { return accumulation! }
    
    mutating func valueOccurred(_ value: Value) {
        if let accumulatedValue = accumulation {
            accumulation = combine(accumulatedValue, value)
        } else {
            accumulation = value
        }
    }
    
    mutating func separatorOccurred(_ separator: @escaping Combine) {
        combine = separator
    }
}

private struct ValueReverseReducer<Value>: Aggregator, SeparatorHandling {
    typealias Combine = (Value, Value) -> Value
    
    var values: [Value] = []
    var combines: [Combine] = []
    var result: Value {
        guard let lastValue = values.last else { fatalError("values is empty") }
        return zip(combines.reversed(), values.dropLast().reversed())
            .reduce(lastValue) { $1.0($1.1, $0) }
    }
    
    mutating func valueOccurred(_ value: Value) {
        values.append(value)
    }
    
    mutating func separatorOccurred(_ separator: @escaping Combine) {
        combines.append(separator)
    }
}



