
// MARK: - Reading the input stream position and handling the user state

public func position() -> Parser<CharacterStream.Position> {
    return Parser { stream in Reply(stream.position) }
}

public func userInfo() -> Parser<CharacterStream.UserInfo> {
    return Parser { stream in Reply(stream.userInfo) }
}

public func updateUserInfo(_ userInfo: CharacterStream.UserInfo) -> Parser<Void> {
    return Parser { stream in
        stream.userInfo = userInfo
        return Reply(())
    }
}

public func updateUserInfo(
    _ transform: @escaping (CharacterStream.UserInfo) -> CharacterStream.UserInfo)
    -> Parser<Void>
{
    return Parser { stream in
        stream.userInfo = transform(stream.userInfo)
        return Reply(())
    }
}

public func userInfoSatisfies(_ predicate: @escaping (CharacterStream.UserInfo) -> Bool)
    -> Parser<Void>
{
    return Parser { stream in
        return predicate(stream.userInfo) ? .success((), []) : .failure([])
    }
}

// MARK: - Parsing single chars

public func character(_ c: Character) -> Parser<Character> {
    return character(c, result: c)
}

public func skipCharacter(_ c: Character) -> Parser<Void> {
    return character(c, result: ())
}

public func character<T>(_ c: Character, result: T) -> Parser<T> {
    if Text.isNewlineChar(c) {
        return newline(result: result)
    }
    return Parser { stream in
        return stream.skip(c) ? Reply(result) : .failure([.Expected(String(c))])
    }
}

public func anyCharacter() -> Parser<Character> {
    return Parser { stream in
        if let c = stream.readCharacter() {
            return Reply(c)
        }
        return .failure([ParserError.expectedAnyCharacter])
    }
}

public func skipAnyCharacter() -> Parser<Void> {
    return Parser { stream in
        if let _ = stream.readCharacter() {
            return Reply(())
        }
        return .failure([ParserError.expectedAnyCharacter])
    }
}

public func satisfy(_ predicate: @escaping (Character) -> Bool, errorLabel: String?)
    -> Parser<Character>
{
    return satisfy(predicate, errors: errorLabel.map { [.Expected($0)] } ?? [])
}

public func skipSatisfy(_ predicate: @escaping (Character) -> Bool, errorLabel: String?)
    -> Parser<Void>
{
    return satisfy(predicate, errorLabel: errorLabel) >>% ()
}

private func satisfy(
    _ predicate: @escaping (Character) -> Bool, errors: [ParserError], handleNewline: Bool = true)
    -> Parser<Character>
{
    if !handleNewline {
        return Parser { stream in
            if let c = stream.peek(), predicate(c) {
                stream.skip()
                return .success(c, [])
            }
            return .failure(errors)
        }
    } else {
        return Parser { stream in
            switch stream.peek() {
            case var c?:
                if Text.isNewlineChar(c) { c = "\n" }
                if predicate(c) {
                    if c != "\n" { stream.skip() }
                    else { stream.skipNewline() }
                    return .success(c, [])
                }
                return .failure(errors)
            default:
                return .failure(errors)
            }
        }
    }
}

public func any<S: Sequence>(of characters: S) -> Parser<Character>
    where S.Iterator.Element == Character
{
    return satisfy(isAny(of: characters), errors: [ParserError.expectedAnyCharacterIn(characters)])
}

public func skipAny<S: Sequence>(of characters: S) -> Parser<Void>
    where S.Iterator.Element == Character
{
    return any(of: characters) >>% ()
}

public func none<S: Sequence>(of characters: S) -> Parser<Character>
    where S.Iterator.Element == Character
{
    return satisfy(isNone(of: characters),
                   errors: [ParserError.expectedAnyCharacterNotIn(characters)])
}

public func skipNone<S: Sequence>(of characters: S) -> Parser<Void>
    where S.Iterator.Element == Character
{
    return none(of: characters) >>% ()
}

public func asciiUppercaseLetter() -> Parser<Character> {
    return satisfy(isASCIIUppercaseLetter, errors: [ParserError.expectedASCIIUppercaseLetter],
                   handleNewline: false)
}

public func asciiLowercaseLetter() -> Parser<Character> {
    return satisfy(isASCIILowercaseLetter, errors: [ParserError.expectedASCIILowercaseLetter],
                   handleNewline: false)
}

public func asciiLetter() -> Parser<Character> {
    return satisfy(isASCIILetter, errors: [ParserError.expectedASCIILetter], handleNewline: false)
}

public func decimalDigit() -> Parser<Character> {
    return satisfy(isDecimalDigit, errors: [ParserError.expectedDecimalDigit], handleNewline: false)
}

public func hexadecimalDigit() -> Parser<Character> {
    return satisfy(isHexadecimalDigit, errors: [ParserError.expectedHexadecimalDigit],
                   handleNewline: false)
}

public func octalDigit() -> Parser<Character> {
    return satisfy(isOctalDigit, errors: [ParserError.expectedOctalDigit], handleNewline: false)
}

public func binaryDigit() -> Parser<Character> {
    return satisfy(isBinaryDigit, errors: [ParserError.expectedBinaryDigit], handleNewline: false)
}

// MARK: - Predicate functions corresponding to the above parsers

public func isAny<S: Sequence>(of characters: S)
    -> (Character) -> Bool where S.Iterator.Element == Character
{
    return { characters.contains($0) }
}

public func isNone<S: Sequence>(of characters: S)
    -> (Character) -> Bool where S.Iterator.Element == Character
{
    return { !characters.contains($0) }
}

public func isASCIIUppercaseLetter(_ c: Character) -> Bool {
    return ("A"..."Z").contains(c)
}

public func isASCIILowercaseLetter(_ c: Character) -> Bool {
    return ("a"..."z").contains(c)
}

public func isASCIILetter(_ c: Character) -> Bool {
    return isASCIIUppercaseLetter(c) || isASCIILowercaseLetter(c)
}

// TODO: 유니코드 캐릭터 지원은 다른 파일에서 NSCharacterSet을 이용해서 구현

public func isDecimalDigit(_ c: Character) -> Bool {
    return ("0"..."9").contains(c)
}

public func isHexadecimalDigit(_ c: Character) -> Bool {
    return ("0"..."9").contains(c) || ("A"..."F").contains(c) || ("a"..."f").contains(c)
}

public func isOctalDigit(_ c: Character) -> Bool {
    return ("0"..."7").contains(c)
}

public func isBinaryDigit(_ c: Character) -> Bool {
    return c == "0" || c == "1"
}

// MARK: - Parsing whitespace

public func tab() -> Parser<Character> {
    return satisfy({ $0 == "\t" }, errors: [ParserError.expectedTab], handleNewline: false)
}

public func newline<T>(result: T) -> Parser<T> {
    return Parser { stream in
        return stream.skipNewline() ? Reply(result) : .failure([ParserError.expectedNewline])
    }
}

public func newline() -> Parser<Character> {
    return newline(result: "\n")
}

public func skipNewline() -> Parser<Void> {
    return newline(result: ())
}

public func skipWhitespaces(atLeastOnce: Bool = false) -> Parser<Void> {
    if atLeastOnce {
        return Parser { stream in
            if stream.skipWhitespaces() { return .success((), []) }
            else { return .failure([ParserError.expectedWhitespace]) }
        }
    } else {
        return Parser { stream in
            stream.skipWhitespaces()
            return .success((), [])
        }
    }
}

public func endOfStream() -> Parser<Void> {
    return Parser { stream in
        if stream.isAtEndOfStream { return .success((), []) }
        else { return .failure([ParserError.expectedEndOfStream]) }
    }
}

// MARK: - Parsing strings directly

public func string(_ str: String, caseInsensitive: Bool = false) -> Parser<String> {
    return _string(str, caseInsensitive: caseInsensitive, result: str)
}

public func skipString(_ str: String, caseInsensitive: Bool = false) -> Parser<Void> {
    return _string(str, caseInsensitive: caseInsensitive, result: ())
}

public func string<T>(_ str: String, caseInsensitive: Bool = false, result: T) -> Parser<T> {
    return _string(str, caseInsensitive: caseInsensitive, result: result)
}

private func _string<T>(
    _ str: String, caseInsensitive: Bool, result: T, function: String = #function)
    -> Parser<T>
{
    checkStringNotContainNewline(str, function: function)
    return Parser { stream in
        if stream.skip(str, caseInsensitive: caseInsensitive) {
            return .success(result, [])
        }
        return .failure([ParserError.ExpectedString(str)])
    }
}

private func checkStringNotContainNewline(_ str: String, function: String) {
    precondition(!str.characters.contains(where: Text.isNewlineChar),
                 "The str argument to '\(function)' may not contain newline characters.")
}

public func anyString(count: Int) -> Parser<String> {
    return Parser { stream in
        let state = stream.state
        let str = stream.readSubstring(minCount: count, maxCount: count,
            normalizingNewlines: true)
        if !str.isEmpty { return .success(str, []) }
        stream.backtrack(to: state)
        return .failure([ParserError.expectedAnyString(count: count)])
    }
}

public func skipAnyString(count: Int) -> Parser<Void> {
    return Parser { stream in
        let state = stream.state
        if count == stream.skipSubstring(minCount: count, maxCount: count).count {
            return .success((), [])
        }
        stream.backtrack(to: state)
        return .failure([ParserError.expectedAnyString(count: count)])
    }
}

public func restOfLine(thenSkippingNewline skippingNewline: Bool = true) -> Parser<String> {
    return Parser { stream in
        return .success(stream.readRestOfLine(thenSkippingNewline: skippingNewline), [])
    }
}

public func skipRestOfLine(thenSkippingNewline skippingNewline: Bool = true) -> Parser<Void> {
    return Parser { stream in
        stream.skipRestOfLine(thenSkippingNewline: skippingNewline)
        return .success((), [])
    }
}

public func string(until
    string: String, caseInsensitive: Bool = false, maxCount: Int = .max, skippingString: Bool)
    -> Parser<String>
{
    checkStringNotContainNewline(string, function: #function)
    precondition(maxCount >= 0, "maxCount is negative")
    return Parser { stream in
        let (substr, _) = stream.readSubstring(until: string, maxCount: maxCount,
            caseInsensitive: caseInsensitive, normalizingNewlines: true)
        guard let substring = substr else {
            return .failure([ParserError.couldNotFindString(string)])
        }
        if skippingString { stream.skip(string.characters.count) }
        return .success(substring, [])
    }
}

public func skipString(until
    string: String, caseInsensitive: Bool = false, maxCount: Int = .max, skippingString: Bool)
    -> Parser<Void>
{
    checkStringNotContainNewline(string, function: #function)
    precondition(maxCount >= 0, "maxCount is negative")
    return Parser { stream in
        let (found, _) = stream.skipSubstring(until: string, maxCount: maxCount,
            caseInsensitive: caseInsensitive)
        guard found else {
            return .failure([ParserError.couldNotFindString(string)])
        }
        if skippingString { stream.skip(string.characters.count) }
        return .success((), [])
    }
}

public func manyCharacters(minCount: Int = 0, maxCount: Int = .max, errorLabel: String? = nil,
    while predicate: @escaping (Character) -> Bool)
    -> Parser<String>
{
    return manyCharacters(minCount: minCount, maxCount: maxCount, errorLabel: errorLabel,
                          first: predicate, while: predicate)
}

public func manyCharacters(minCount: Int = 0, maxCount: Int = .max, errorLabel: String? = nil,
    first firstPredicate: @escaping (Character) -> Bool,
    while predicate: @escaping (Character) -> Bool)
    -> Parser<String>
{
    let errors: [ParserError] = errorLabel.map { [.Expected($0)] } ?? []
    return Parser { stream in
        let substr = stream.readSubstring(minCount: minCount, maxCount: maxCount,
            normalizingNewlines: true, first: firstPredicate, while: predicate)
        return substr.characters.count < minCount ? .failure(errors) : .success(substr, [])
    }
}

public func skipManyCharacters(minCount: Int = 0, maxCount: Int = .max, errorLabel: String? = nil,
    while predicate: @escaping (Character) -> Bool)
    -> Parser<Void>
{
    return skipManyCharacters(minCount: minCount, maxCount: maxCount, errorLabel: errorLabel,
                              first: predicate, while: predicate)
}

public func skipManyCharacters(minCount: Int = 0, maxCount: Int = .max, errorLabel: String? = nil,
    first firstPredicate: @escaping (Character) -> Bool,
    while predicate: @escaping (Character) -> Bool)
    -> Parser<Void>
{
    let errors: [ParserError] = errorLabel.map { [.Expected($0)] } ?? []
    return Parser { stream in
        let bounds = stream.skipSubstring(minCount: minCount, maxCount: maxCount,
            first: firstPredicate, while: predicate)
        return bounds.count < minCount ? .failure(errors) : .success((), [])
    }
}

public func regex(_ pattern: String, errorLabel: String? = nil) -> Parser<String> {
    let expression: NSRegularExpression
    do {
        try expression = NSRegularExpression(pattern: pattern, options: [])
    } catch {
        fatalError("regex pattern \"\(pattern)\" is invalid")
    }
    let errors: [ParserError] = errorLabel.map { [.Expected($0)] }
        ?? [ParserError.expectedMatchingRegex(pattern)]
    
    return Parser { stream in
        guard let matchRange = stream.match(expression)?.range else {
            return .failure(errors)
        }
        let substr = (stream.string as NSString).substring(with: matchRange)
        if !substr.characters.contains(where: Text.isNewlineChar) {
            stream.skip(substr.characters.count)
            return .success(substr, [])
        }
        stream.skipSubstring(maxCount: substr.characters.count)
        return .success(Text.normalizingNewlines(of: substr), [])
    }
}

// MARK: - Parsing strings with the help of other parsers

public func manyCharacters(_ parser: Parser<Character>, atLeastOnce: Bool = false)
    -> Parser<String>
{
    return manyCharacters(first: parser, while: parser, atLeastOnce: atLeastOnce)
}

public func manyCharacters(
    first firstParser: Parser<Character>, while parser: Parser<Character>,
    atLeastOnce: Bool = false)
    -> Parser<String>
{
    return many(first: firstParser, while: parser, atLeastOnce: atLeastOnce,
                aggregatorGenerator: CharacterAggregator.init)
}

private struct CharacterAggregator: Aggregator {
    var result: String = ""
    mutating func valueOccurred(_ value: Character) {
        result.append(value)
    }
}

public func manyCharacters<T>(first
    firstParser: Parser<Character>, while parser: Parser<Character>,
    until endParser: Parser<T>, atLeastOnce: Bool = false)
    -> Parser<String>
{
    return manyCharacters(first: firstParser, while: parser, until: endParser,
                          atLeastOnce: atLeastOnce, apply: { $0.0 })
}

public func manyCharacters<T1, T2>(first
    firstParser: Parser<Character>, while parser: Parser<Character>,
    until endParser: Parser<T1>, atLeastOnce: Bool = false,
    apply transform: @escaping (String, T1) -> T2)
    -> Parser<T2>
{
    return many(first: firstParser, while: parser, until: endParser,
                atLeastOnce: atLeastOnce, apply: transform,
                aggregatorGenerator: CharacterAggregator.init)
}

public func manyStrings(_ parser: Parser<String>, atLeastOnce: Bool = false) -> Parser<String> {
    return manyStrings(first: parser, while: parser, atLeastOnce: atLeastOnce)
}

public func manyStrings(
    first firstParser: Parser<String>, while parser: Parser<String>, atLeastOnce: Bool = false)
    -> Parser<String>
{
    return many(first: firstParser, while: parser, atLeastOnce: atLeastOnce,
                aggregatorGenerator: StringAggregator.init)
}

private struct StringAggregator: Aggregator {
    var result: String = ""
    mutating func valueOccurred(_ value: String) {
        result.append(value)
    }
}

public func manyStrings(
    _ p: Parser<String>, separator: Parser<String>, includeSeparator: Bool)
    -> Parser<String>
{
    let aggregatorGenerator = { StringSeparatorAggregator.init(includeSeparator: includeSeparator) }
    return many(p, separator: separator, atLeastOnce: false, allowEndBySeparator: false,
                aggregatorGenerator: aggregatorGenerator)
}

private final class StringSeparatorAggregator: Aggregator, SeparatorHandling {
    var result: String = ""
    var separatorHandler: (String) -> () = { _ in }
    
    init(includeSeparator: Bool) {
        if includeSeparator {
            self.separatorHandler = { [unowned self] in self.result.append($0) }
        }
    }
    
    func valueOccurred(_ value: String) {
        result.append(value)
    }
    func separatorOccurred(_ separator: String) {
        separatorHandler(separator)
    }
}

public func stringSkipped(by p: Parser<Void>) -> Parser<String> {
    return Parser { stream in
        let index = stream.nextIndex
        let line = stream.lineNumber
        switch p.parse(stream) {
        case let .success(_, e):
            let str = stream.read(from: index)
            let normalizedStr = line == stream.lineNumber ? str : Text.normalizingNewlines(of: str)
            return .success(normalizedStr, e)
        case let .failure(e):
            return .failure(e)
        case let .fatalFailure(e):
            return .fatalFailure(e)
        }
    }
}

public func skip<T1, T2>(_ p: Parser<T1>, apply transform: @escaping (String, T1) -> T2)
    -> Parser<T2>
{
    return Parser { stream in
        let index = stream.nextIndex
        let line = stream.lineNumber
        switch p.parse(stream) {
        case let .success(v, e):
            let str = stream.read(from: index)
            let normalizedStr = line == stream.lineNumber ? str : Text.normalizingNewlines(of: str)
            return .success(transform(normalizedStr, v), e)
        case let .failure(e):
            return .failure(e)
        case let .fatalFailure(e):
            return .fatalFailure(e)
        }
    }
}

// MARK: - Conditional parsing

public func notFollowedByEndOfStream() -> Parser<Void> {
    return Parser { stream in
        if !stream.isAtEndOfStream { return .success((), []) }
        else { return .failure([ParserError.unexpectedEndOfStream]) }
    }
}

public func followedByNewline() -> Parser<Void> {
    return Parser { stream in
        switch stream.peek() {
        case let c? where Text.isNewlineChar(c):
            return .success((), [])
        default:
            return .failure([ParserError.expectedNewline])
        }
    }
}

public func notFollowedByNewline() -> Parser<Void> {
    return Parser { stream in
        switch stream.peek() {
        case let c? where Text.isNewlineChar(c):
            return .failure([ParserError.unexpectedNewline])
        default:
            return .success((), [])
        }
    }
}

public func followed(by str: String, caseInsensitive: Bool = false) -> Parser<Void> {
    checkStringNotContainNewline(str, function: #function)
    return Parser { stream in
        if stream.match(str, caseInsensitive: caseInsensitive) {
            return .success((), [])
        }
        return .failure([ParserError.ExpectedString(str)])
    }
}

public func notFollowed(by str: String, caseInsensitive: Bool = false) -> Parser<Void> {
    checkStringNotContainNewline(str, function: #function)
    return Parser { stream in
        if !stream.match(str, caseInsensitive: caseInsensitive) {
            return .success((), [])
        }
        return .failure([ParserError.UnexpectedString(str)])
    }
}

public func followed(by predicate: @escaping (Character) -> Bool) -> Parser<Void> {
    return Parser { stream in
        if let c = stream.peek(), predicate(c) {
            return .success((), [])
        }
        return .failure([])
    }
}

public func notFollowed(by predicate: @escaping (Character) -> Bool) -> Parser<Void> {
    return Parser { stream in
        if let c = stream.peek(), predicate(c) {
            return .failure([])
        }
        return .success((), [])
    }
}

public func followed(by predicate: @escaping (Character, Character) -> Bool) -> Parser<Void> {
    return Parser { stream in
        if let c0 = stream.peek(), let c1 = stream.peek(offset: 1), predicate(c0, c1) {
            return .success((), [])
        }
        return .failure([])
    }
}

public func notFollowed(by predicate: @escaping (Character, Character) -> Bool) -> Parser<Void> {
    return Parser { stream in
        if let c0 = stream.peek(), let c1 = stream.peek(offset: 1), predicate(c0, c1) {
            return .failure([])
        }
        return .success((), [])
    }
}

public func preceded(by predicate: @escaping (Character) -> Bool) -> Parser<Void> {
    return Parser { stream in
        if let c = stream.peek(offset: -1), predicate(c) {
            return .success((), [])
        }
        return .failure([])
    }
}

public func notPreceded(by predicate: @escaping (Character) -> Bool) -> Parser<Void> {
    return Parser { stream in
        if let c = stream.peek(offset: -1), predicate(c) {
            return .failure([])
        }
        return .success((), [])
    }
}



