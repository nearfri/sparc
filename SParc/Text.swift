
public enum Text {
    public static func isBlankChar(_ c: Character) -> Bool {
        switch c {
        case " ", "\t":
            return true
        default:
            return false
        }
    }
    
    public static func isNewlineChar(_ c: Character) -> Bool {
        switch c {
        case "\n", "\r", "\r\n", "\u{000B}", "\u{000C}", "\u{0085}", "\u{2028}", "\u{2029}":
            return true
        default:
            return false
        }
    }
    
    public static func isWhitespaceChar(_ c: Character) -> Bool {
        return isBlankChar(c) || isNewlineChar(c)
    }
    
    public static func normalizingNewlines(of string: String) -> String {
        var result = ""
        result.reserveCapacity(string.characters.count)
        for c in string.characters {
            result.append(isNewlineChar(c) ? "\n" : c)
        }
        return result
    }
}



