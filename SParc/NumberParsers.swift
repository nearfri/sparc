
// MARK: - Parsing numbers

public struct NumberLiteral {
    public fileprivate(set) var string: String = ""
    public fileprivate(set) var suffix: String = ""
    public fileprivate(set) var sign: Sign = .none
    public fileprivate(set) var classification: Classification = .finite
    public fileprivate(set) var notation: Notation = .decimal
    public fileprivate(set) var hasIntegerPart: Bool = false
    public fileprivate(set) var hasFractionalPart: Bool = false
    public fileprivate(set) var hasExponent: Bool = false
    
    fileprivate static let maxSuffixCharacterCount = 4
}

extension NumberLiteral: Equatable {
    public static func ==(lhs: NumberLiteral, rhs: NumberLiteral) -> Bool {
        return lhs.string == rhs.string
            && lhs.suffix == rhs.suffix
            && lhs.sign == rhs.sign
            && lhs.classification == rhs.classification
            && lhs.hasIntegerPart == rhs.hasIntegerPart
            && lhs.hasFractionalPart == rhs.hasFractionalPart
            && lhs.hasExponent == rhs.hasExponent
            && lhs.notation == rhs.notation
    }
}

extension NumberLiteral {
    public var hasSign: Bool {
        return sign != .none
    }
    
    public var isInteger: Bool {
        return !hasFractionalPart && !hasExponent
    }
    
    public var radix: Int {
        return notation.rawValue
    }
}

extension NumberLiteral {
    public enum Sign {
        case none
        case plus
        case minus
    }
    
    public enum Notation: Int {
        case decimal = 10       // no prefix
        case binary = 2         // 0b prefix
        case octal = 8          // 0o prefix
        case hexadecimal = 16   // 0x prefix
    }
    
    public enum Classification {
        case finite
        case nan
        case infinity
    }
    
    public struct Options: OptionSet {
        public let rawValue: Int
        public init(rawValue: Int) {
            self.rawValue = rawValue
        }
        
        public static let allowPlusSign = Options(rawValue: 1 << 0)
        public static let allowMinusSign = Options(rawValue: 1 << 1)
        public static let allowHexadecimal = Options(rawValue: 1 << 2)
        public static let allowOctal = Options(rawValue: 1 << 3)
        public static let allowBinary = Options(rawValue: 1 << 4)
        public static let allowFractionalPart = Options(rawValue: 1 << 5)
        public static let allowNoIntegerPart = Options(rawValue: 1 << 6)
        public static let allowExponent = Options(rawValue: 1 << 7)
        public static let allowNaN = Options(rawValue: 1 << 8)
        public static let allowInfinity = Options(rawValue: 1 << 9)
        public static let allowSuffix = Options(rawValue: 1 << 10)
        
        public static let defaultInteger: Options = [allowPlusSign, allowMinusSign,
                                                     allowHexadecimal, allowOctal, allowBinary]
        public static let defaultFloat: Options = [allowPlusSign, allowMinusSign, allowHexadecimal,
                                                   allowFractionalPart, allowNoIntegerPart,
                                                   allowExponent, allowNaN, allowInfinity]
    }
}

public func numberLiteral(_ options: NumberLiteral.Options, errorLabel: String)
    -> Parser<NumberLiteral>
{
    return numberLiteral(options, errorWhenNoLiteral: .Expected(errorLabel))
}

public func numberLiteral(
    _ options: NumberLiteral.Options, errorWhenNoLiteral: ParserError? = nil)
    -> Parser<NumberLiteral>
{
    return Parser { stream in
        return NumberLiteralParser.parse(stream, options: options,
            errorWhenNoLiteral: errorWhenNoLiteral ?? ParserError.expectedNumber)
    }
}

public func float() -> Parser<Double> {
    return float(.defaultFloat)
}

public func float(_ options: NumberLiteral.Options) -> Parser<Double> {
    return Parser { stream in
        let parser: Parser<NumberLiteral> = numberLiteral(options,
            errorWhenNoLiteral: ParserError.expectedFloat)
        let index = stream.nextIndex
        switch parser.parse(stream) {
        case let .success(literal, _):
            if let value = Double(literal.string) {
                return .success(value, [])
            }
            stream.seek(to: index)
            return .fatalFailure([.overflow(numberString: literal.string)])
        case let .failure(e):
            return .failure(e)
        case let .fatalFailure(e):
            return .fatalFailure(e)
        }
    }
}

public func integer() -> Parser<Int> {
    return Parser { stream in
        let parser: Parser<NumberLiteral> = numberLiteral(.defaultInteger,
            errorWhenNoLiteral: ParserError.expectedInteger)
        let index = stream.nextIndex
        switch parser.parse(stream) {
        case let .success(literal, _):
            var str = literal.string
            if literal.notation != .decimal {
                let startIndex = literal.hasSign ? str.index(after: str.startIndex) : str.startIndex
                str.removeSubrange(startIndex..<str.index(startIndex, offsetBy: 2))
            }
            if let value = Int(str, radix: literal.radix) {
                return .success(value, [])
            }
            stream.seek(to: index)
            return .fatalFailure([.overflow(numberString: literal.string)])
        case let .failure(e):
            return .failure(e)
        case let .fatalFailure(e):
            return .fatalFailure(e)
        }
    }
}

private final class NumberLiteralParser {
    let stream: CharacterStream
    let options: NumberLiteral.Options
    var numberLiteral = NumberLiteral()
    var error: ParserError?
    var errorWhenNoLiteral: ParserError
    
    init(stream: CharacterStream, options: NumberLiteral.Options,
         errorWhenNoLiteral: ParserError)
    {
        self.stream = stream
        self.options = options
        self.errorWhenNoLiteral = errorWhenNoLiteral
    }
    
    static func parse(_ stream: CharacterStream, options: NumberLiteral.Options,
                      errorWhenNoLiteral: ParserError)
        -> Reply<NumberLiteral, ParserError>
    {
        return NumberLiteralParser(stream: stream, options: options,
                                   errorWhenNoLiteral: errorWhenNoLiteral).parse()
    }
    
    func parse() -> Reply<NumberLiteral, ParserError> {
        parseNumber()
        
        if let error = error {
            return .failure([error])
        }
        return .success(numberLiteral, [])
    }
    
    func parseNumber() {
        let index = stream.nextIndex
        let stateTag = stream.stateTag
        
        parseSign()
        
        if parseNaN() || parseInfinity() {
            numberLiteral.string = stream.read(from: index)
            return
        }
        
        guard parseNotation() else {
            backtrack(to: index, stateTag: stateTag)
            return
        }
        
        parseDigits()
        
        guard error == nil else { return }
        
        numberLiteral.string = stream.read(from: index)
        
        parseSuffix()
    }
    
    func backtrack(to index: CharacterStream.Index, stateTag: Int) {
        stream.seek(to: index)
        stream.stateTag = stateTag
    }
    
    func parseSign() {
        switch stream.peek() {
        case "+"? where options.contains(.allowPlusSign):
            numberLiteral.sign = .plus
            stream.skip()
        case "-"? where options.contains(.allowMinusSign):
            numberLiteral.sign = .minus
            stream.skip()
        default:
            break
        }
    }
    
    func parseNaN() -> Bool {
        guard options.contains(.allowNaN) && stream.skip("nan", caseInsensitive: true) else {
            return false
        }
        numberLiteral.classification = .nan
        return true
    }
    
    func parseInfinity() -> Bool {
        guard options.contains(.allowInfinity) && stream.skip("inf", caseInsensitive: true) else {
            return false
        }
        numberLiteral.classification = .infinity
        stream.skip("inity", caseInsensitive: true)
        return true
    }
    
    func parseNotation() -> Bool {
        if options.contains(.allowHexadecimal) && stream.skip("0x", caseInsensitive: true) {
            numberLiteral.notation = .hexadecimal
            return true
        }
        if options.contains(.allowOctal) && stream.skip("0o", caseInsensitive: true) {
            numberLiteral.notation = .octal
            return true
        }
        if options.contains(.allowBinary) && stream.skip("0b", caseInsensitive: true) {
            numberLiteral.notation = .binary
            return true
        }
        let allowPointStart = self.options.contains([.allowFractionalPart, .allowNoIntegerPart])
        if stream.match(isDecimalDigit) || (allowPointStart && stream.match(".")) {
            numberLiteral.notation = .decimal
            return true
        }
        error = errorWhenNoLiteral
        return false
    }
    
    func parseDigits() {
        switch numberLiteral.notation {
        case .hexadecimal:
            parseHexadecimal()
        case .octal:
            parseOctal()
        case .binary:
            parseBinary()
        case .decimal:
            parseDecimal()
        }
    }
    
    func parseDecimal() {
        parseIntegerPart(isDecimalDigit)
        guard numberLiteral.hasIntegerPart || options.contains(.allowNoIntegerPart) else {
            error = ParserError.expectedDecimalDigit
            return
        }
        
        parseFractionalPart(isDecimalDigit)
        guard numberLiteral.hasIntegerPart || numberLiteral.hasFractionalPart else {
            error = errorWhenNoLiteral
            return
        }
        
        parseExponent("e")
    }
    
    func parseHexadecimal() {
        parseIntegerPart(isHexadecimalDigit)
        guard numberLiteral.hasIntegerPart || options.contains(.allowNoIntegerPart) else {
            error = ParserError.expectedHexadecimalDigit
            return
        }
        
        parseFractionalPart(isHexadecimalDigit)
        guard numberLiteral.hasIntegerPart || numberLiteral.hasFractionalPart else {
            error = ParserError.expectedHexadecimalDigit
            return
        }
        
        parseExponent("p")
    }
    
    func parseOctal() {
        parseIntegerPart(isOctalDigit)
        if !numberLiteral.hasIntegerPart {
            error = ParserError.expectedOctalDigit
        }
    }
    
    func parseBinary() {
        parseIntegerPart(isBinaryDigit)
        if !numberLiteral.hasIntegerPart {
            error = ParserError.expectedBinaryDigit
        }
    }
    
    func parseIntegerPart(_ digitPredicate: (Character) -> Bool) {
        numberLiteral.hasIntegerPart = stream.skipSubstring(while: digitPredicate).count > 0
    }
    
    func parseFractionalPart(_ digitPredicate: (Character) -> Bool) {
        guard options.contains(.allowFractionalPart) && stream.skip("." as Character) else {
            return
        }
        if stream.skipSubstring(while: digitPredicate).count > 0 || numberLiteral.hasIntegerPart {
            numberLiteral.hasFractionalPart = true
        }
    }
    
    func parseExponent(_ exp: String) {
        guard options.contains(.allowExponent) && stream.skip(exp, caseInsensitive: true) else {
            return
        }
        numberLiteral.hasExponent = true
        stream.skip { $0 == "+" || $0 == "-" }
        if stream.skipSubstring(while: isDecimalDigit).count == 0 {
            error = ParserError.expectedDecimalDigit
        }
    }
    
    func parseSuffix() {
        guard options.contains(.allowSuffix) else {
            return
        }
        numberLiteral.suffix = stream.readSubstring(minCount: 0,
                                                    maxCount: NumberLiteral.maxSuffixCharacterCount,
                                                    normalizingNewlines: false,
                                                    while: isASCIILetter)
    }
}



