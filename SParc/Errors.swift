
extension ParserError {
    internal static var expectedAnyCharacter: ParserError {
        return .Expected("any character")
    }
    
    internal static func expectedAnyCharacterIn<S: Sequence>(_ characters: S) -> ParserError
        where S.Iterator.Element == Character
    {
        return .Expected("any character in \(characters)")
    }
    
    internal static func expectedAnyCharacterNotIn<S: Sequence>(_ characters: S) -> ParserError
        where S.Iterator.Element == Character
    {
        return .Expected("any character not in \(characters)")
    }
    
    internal static var expectedASCIIUppercaseLetter: ParserError {
        return .Expected("ASCII uppercase letter")
    }
    
    internal static var expectedASCIILowercaseLetter: ParserError {
        return .Expected("ASCII lowercase letter")
    }
    
    internal static var expectedASCIILetter: ParserError {
        return .Expected("ASCII letter")
    }
    
    internal static var expectedDecimalDigit: ParserError {
        return .Expected("decimal digit")
    }
    
    internal static var expectedHexadecimalDigit: ParserError {
        return .Expected("hexadecimal digit")
    }
    
    internal static var expectedOctalDigit: ParserError {
        return .Expected("octal digit")
    }
    
    internal static var expectedBinaryDigit: ParserError {
        return .Expected("binary digit")
    }
    
    internal static var expectedTab: ParserError {
        return .Expected("tab")
    }
    
    internal static var expectedNewline: ParserError {
        return .Expected("newline")
    }
    
    internal static var unexpectedNewline: ParserError {
        return .Unexpected("newline")
    }
    
    internal static var expectedWhitespace: ParserError {
        return .Expected("whitespace")
    }
    
    internal static var expectedEndOfStream: ParserError {
        return .Expected("end of stream")
    }
    
    internal static var unexpectedEndOfStream: ParserError {
        return .Unexpected("end of stream")
    }
    
    internal static func expectedAnyString(count: Int) -> ParserError
    {
        return .Expected("any string of \(count) characters")
    }
    
    internal static func couldNotFindString(_ string: String) -> ParserError {
        return .Other(message: "could not find the string \"\(string)\"")
    }
    
    internal static func expectedMatchingRegex(_ pattern: String) -> ParserError {
        return .Expected("string matching the regex \(pattern)")
    }
    
    internal static var expectedNumber: ParserError {
        return .Expected("number")
    }
    
    internal static var expectedFloat: ParserError {
        return .Expected("floating-point number")
    }
    
    internal static var expectedInteger: ParserError {
        return .Expected("integer number")
    }
    
    internal static func overflow(numberString: String) -> ParserError {
        return .Other(message: "\(numberString) is outside the allowable range")
    }
}



