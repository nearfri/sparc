
import XCTest
import SParc
@testable import ParserKit

class JavascriptTokenizerTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testJSTokenizer() {
        let str = type(of: self).loadString("tokens", "js")
        let stream = CharacterStream(string: str)
        let parser = javascriptTokenizer()
        
        loop: repeat {
            switch parser.parse(stream) {
            case let .success(v, _):
                print(v)
                if v.type == .end {
                    break loop
                }
            case let .failure(e):
                print("failure \(e)")
                break loop
            case let .fatalFailure(e):
                print("fatalFailure \(e)")
                break loop
            }
        } while true
    }
}



