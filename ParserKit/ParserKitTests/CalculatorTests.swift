
import XCTest
import SParc
@testable import ParserKit

class CalculatorTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testTokenizer() {
        let str = "3 * (2 + -4) ^ 4"
        let stream = CharacterStream(string: str)
        let tokenizer = makeCalculatorTokenizer()
        
        print("\n\n")
        loop: repeat {
            switch tokenizer.parse(stream) {
            case let .success(v, _):
                print(v)
                if v.type == .end {
                    break loop
                }
            case let .failure(e):
                print("failure \(e)")
                break loop
            case let .fatalFailure(e):
                print("fatalFailure \(e)")
                break loop
            }
        } while true
        print("\n\n")
    }
    
    func testCalc() {
        let str = "+2*pow(+3 * (+2 + -4) ^ +4, 3) / -2"
        let calc = calculator()
        let ret = SParc.run(parser: calc,
                            string: str)
        print("\n")
        switch ret {
        case let .success(value, _, _):
            print(str + " = " + String(value))
        default:
            print(ret)
        }
        print("\n")
    }
}



