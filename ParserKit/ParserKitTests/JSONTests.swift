
import XCTest
import SParc
@testable import ParserKit

private let loopCount = 50

class JSONTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let ret = SParc.run(parser: json(), string: type(of: self).loadString("test4", "json"))
        print(ret)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        let str = type(of: self).loadString("test4", "json")
        self.measure {
            _ = SParc.run(parser: json(), string: str)
        }
    }
    
    func testPerformance_SParc() {
        let str = type(of: self).loadString("test4", "json")
        let parser = json()
        self.measure {
            for _ in 1...loopCount {
                let stream = CharacterStream(string: str)
                let _ = parser.parse(stream)
            }
        }
    }
    
    func testPerformance_NSJSONSerialization() {
        let str = type(of: self).loadString("test4", "json")
        self.measure {
            for _ in 1...loopCount {
                let jsonData = str.data(using: String.Encoding.utf8)!
                let _ = (try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? NSDictionary
            }
        }
    }
}



