
import XCTest

extension XCTestCase {
    class func loadString(
        _ name: String, _ ext: String, file: StaticString = #file,
        function: String = #function, line: Int = #line)
        -> String
    {
        guard let string = Bundle(for: self)
            .url(forResource: name, withExtension: ext)
            .flatMap({ try? String(contentsOf: $0, encoding: String.Encoding.utf8) }) else
        {
            fatalError("\(function): No file (\(name).\(ext)) found")
        }
        
        return string
    }
}



