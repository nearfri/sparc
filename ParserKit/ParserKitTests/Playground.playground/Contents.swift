//: Playground - noun: a place where people can play

import Cocoa
import SParc
import ParserKit
import CoreMedia

/*
 https://www.w3schools.com/xml/xml_elements.asp
 XML elements must follow these naming rules:
 
 Element names are case-sensitive
 Element names must start with a letter or underscore
 Element names cannot start with the letters xml (or XML, or Xml, etc)
 Element names can contain letters, digits, hyphens, underscores, and periods
 Element names cannot contain spaces
 Any name can be used, no words are reserved (except xml).
 위에 나온대로 이름을 파싱하는건 성능 상 문제가 되니 whitespace와 >, / 기호 정도만 가지고 파싱한다
 
 Empty elements can have attributes.
 
 <title >   valid
 < title>   invalid
 </title>   valid
 < /title>  invalid
 </ title>  invalid
 <title />  valid
 <title/>   valid
 <title/ >  invalid
 <title / > invalid
 <title asdf = "qwer">  valid
 <title asdf = "qewr"/> valid
 <title asdf = "qwer" /> valid
 
 end: "<" >>! "/" >>! elementName !>> ws !>> ">" |>> { .end($0) }
 start or self-closing: "<" >>! pipe(elementName !>> ws, attributes(), ">" <|> "/>") { .start($0) }
 
 type: start or end or selfClosing
 
 */

let html1 = "<P Class=KRCC><font color=\"#79F3AE\">자막 색상</font></P>"
let html2 = "<a href=https://www.w3schools.com>This is a link</a>"
let xml1 = "<person gender=\"female\">"
let xml2 = "<person gender='female'>"
let xml3 = "<gangster name='George \"Shotgun\" Ziegler'>"
let xml4 = "<gangster name=\"George &quot;Shotgun&quot; Ziegler\">"
let xml5 = "<element></element>" // empty element
let xml6 = "<element />" // self-closing tag
let xml7 = "</element>"
let xml8 = "<!--\n"
    + "P {margin-left:8pt; margin-right:8pt; margin-bottom:2pt; margin-top:2pt;\n"
    + "text-align:center; font-size:20pt; font-family:arial, sans-serif;\n"
    + "font-weight:normal; color:White;}\n"
    + ".KRCC {Name:한국어; lang:kr-KR; SAMIType:CC;}\n"
    + ".ENUSCC { name: English; lang: en-US ; SAMIType: CC ; }\n"
    + "-->"
let xml10 = "<note day=\"10\" month=\"01\" year=\"2008\"\n"
    + "to=\"Tove\" from=\"Jani\" heading=\"Reminder\"\n"
    + "body=\"Don't forget me this weekend!\">\n"
    + "</note>"
let xmls = [
    html1, html2, xml1, xml2, xml3, xml4, xml5, xml6, xml7, xml8, xml10
]

let pHTMLTag = htmlTag()
for xml in xmls {
    switch pHTMLTag.run(xml) {
    case let .success(v, _, _):
        print(v)
    case let .failure(e, a, b):
        print(e)
        print(a)
        print(b)
    }
    print("")
}

print("\n\n")

let subripStr = ""
    + "1\n"
    + "00:02:17,440 --> 00:02:20,375  X1:100 X2:100 Y1:100 Y2:100\n"
    + "<font color=\"#0000ff\">Detta handlar om min storebrors</font>\n"
    + "<b><i><u>kriminella <font color=\"SkyBlue\">beteende och foersvinnade.</font></u></i></b>\n"
    + "our final approach into Coruscant.\n"
    + "\n"
    + "2\n"
    + "00:02:20,476 --> 00:02:22,501\n"
    + "Very good, Lieutenant.\n"

var str = "Hello, playground"

func stringFromTime(_ time: CMTime) -> String {
    var seconds = time.seconds
    let hours = Int(seconds) / 3600
    seconds = seconds - Double(hours) * 3600
    let mins = Int(seconds) / 60
    seconds = seconds - Double(mins) * 60
    let secs = Int(seconds.rounded(.down))
    let ms = Int(((seconds - Double(secs)) * 1000).rounded())
    return String(format: "%02d:%02d:%d.%d", hours, mins, secs, ms)
}

func stringFromTimeRange(_ timeRange: CMTimeRange) -> String {
    let start = stringFromTime(timeRange.start)
    let end = stringFromTime(timeRange.end)
    return "\(start) - \(end)"
}

let parser = SubRipParser()
let ret = parser.parse(subripStr)
if let subtitle = ret {
    let track = subtitle.tracks[0]
    for frame in track.frames {
        let txt: String = {
            var ret = ""
            for dialog in frame.dialogs {
                guard let txtDialog = dialog as? SubtitleTextDialog else {
                    continue
                }
                if !ret.isEmpty {
                    ret.append("\n")
                }
                print(txtDialog.text)
                ret.append(txtDialog.text.string)
            }
            return ret
        }()
        print("\(stringFromTimeRange(frame.timeRange))  \"\(txt)\"")
    }
}

