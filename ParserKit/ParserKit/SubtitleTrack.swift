
import Foundation
import CoreMedia

open class SubtitleTrack {
    open var name: String = "" // "Korean Captions"
    open var localeIdentifier: String = "" // "ko_KR"
    open var trackID: CMPersistentTrackID = 1 // kCMPersistentTrackID_Invalid = 0
    open fileprivate(set) var frames: [SubtitleFrame] = []
    fileprivate var recentIndex: Int = 0
    
    open var timeRange: CMTimeRange {
        if let firstFrame = frames.first, let lastFrame = frames.last {
            return CMTimeRange(start: firstFrame.startTime, end: lastFrame.endTime)
        }
        return kCMTimeRangeZero
    }
    
    open func frame(at time: CMTime) -> SubtitleFrame? {
        let frameRange = rangeOfFrame(at: time)
        if !frameRange.isEmpty {
            return frames[frameRange.lowerBound]
        }
        
        let nextFrameIndex = frameRange.lowerBound
        if nextFrameIndex <= frames.startIndex || nextFrameIndex >= frames.endIndex {
            return nil
        }
        
        let timeRange = CMTimeRange(start: frames[nextFrameIndex-1].endTime,
                                    end: frames[nextFrameIndex].startTime)
        return SubtitleFrame(timeRange: timeRange)
    }
    
    open func rangeOfFrame(at time: CMTime) -> CountableRange<Int> {
        if let index = indexOfFrame(at: time, in: recentIndex..<recentIndex+2) {
            recentIndex = index
            return index..<index+1
        }
        
        let range = rangeOfFrame(at: time, in: frames.startIndex..<frames.endIndex)
        recentIndex = range.lowerBound
        return range
    }
    
    private func indexOfFrame(at time: CMTime, in range: CountableRange<Int>) -> Int? {
        let searchRange = range.lowerBound..<min(range.upperBound, frames.endIndex)
        return frames[searchRange].index(where: { $0.timeRange.containsTime(time) })
    }
    
    private func rangeOfFrame(at time: CMTime, in range: CountableRange<Int>)
        -> CountableRange<Int>
    {
        if range.lowerBound == range.upperBound {
            return range // Not found
        }
        let middleBound = (range.lowerBound + range.upperBound) / 2
        let frame = frames[middleBound]
        if frame.timeRange.containsTime(time) {
            return middleBound..<middleBound+1 // Found
        }
        if time < frame.startTime {
            return rangeOfFrame(at: time, in: range.lowerBound..<middleBound)
        }
        return rangeOfFrame(at: time, in: middleBound+1..<range.upperBound)
    }
    
    open func insertFrame(_ frame: SubtitleFrame) {
        if let lastFrame = frames.last, lastFrame.endTime <= frame.startTime {
            frames.append(frame)
            return
        }
        
        let lowerBound = rangeOfFrame(at: frame.startTime).lowerBound
        let upperBound = rangeOfFrame(at: frame.endTime).upperBound
        let overlappedRange = lowerBound..<upperBound
        if overlappedRange.count == 0 {
            frames.insert(frame, at: lowerBound)
            return
        }
        
        let overlappedFrames = Array(frames[overlappedRange]) + [frame]
        let timeStamps = self.timeStamps(from: overlappedFrames)
        var mergedFrames: [SubtitleFrame] = []
        for i in 0..<timeStamps.count-1 {
            let timeRange = CMTimeRange(start: timeStamps[i], end: timeStamps[i+1])
            if let mergedFrame = merging(overlappedFrames, in: timeRange) {
                mergedFrames.append(mergedFrame)
            }
        }
        frames.replaceSubrange(overlappedRange, with: mergedFrames)
    }
    
    private func timeStamps(from frames: [SubtitleFrame]) -> [CMTime] {
        var result: [CMTime] = []
        result.reserveCapacity(frames.count * 2)
        for frame in frames {
            if !result.contains(frame.startTime) {
                result.append(frame.startTime)
            }
            if !result.contains(frame.endTime) {
                result.append(frame.endTime)
            }
        }
        result.sort()
        return result
    }
    
    private func merging(_ overlappedFrames: [SubtitleFrame], in timeRange: CMTimeRange)
        -> SubtitleFrame?
    {
        var matchedFrames: [SubtitleFrame] = []
        for frame in overlappedFrames {
            if frame.timeRange.containsTime(timeRange.start) {
                matchedFrames.append(frame)
            }
        }
        
        guard matchedFrames.count > 0 else { return nil }
        
        matchedFrames.sort { $0.startTime < $1.startTime }
        
        let dialogs = matchedFrames.flatMap { $0.dialogs }
        return SubtitleFrame(dialogs: dialogs, timeRange: timeRange)
    }
    
    open func removeFrame(at index: Int) {
        frames.remove(at: index)
    }
    
    open func removeAllFrames(keepingCapacity keepCapacity: Bool = false) {
        frames.removeAll(keepingCapacity: keepCapacity)
    }
}



