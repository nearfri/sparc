
import Foundation
import SParc

public enum XMLTag {
    case start(name: String, attributes: [String: String])
    case end(name: String)
    case selfClosing(name: String, attributes: [String: String])
    case comment(String)
}

public func xmlTag() -> Parser<XMLTag> {
    let attrs = attributes(attributeValue: xmlAttributeValue(),
                           entityDecoder: { $0.removingXMLEntities })
    return tag(attributes: attrs)
}

public func htmlTag() -> Parser<XMLTag> {
    let attrs = attributes(attributeValue: htmlAttributeValue(),
                           entityDecoder: { $0.removingHTMLEntities })
    return tag(attributes: attrs)
}

private func tag(attributes: Parser<[String: String]>) -> Parser<XMLTag> {
    let startOrSelfClosing = startOrSelfClosingTag(attributes: attributes)
    let end = endTag()
    let comment = commentTag()
    
    return character("<", result: ()) >>! (comment <|> startOrSelfClosing <|> end)
}

private func commentTag() -> Parser<XMLTag> {
    let open = skipString("!--", caseInsensitive: false)
    let close = string(until: "-->", caseInsensitive: false, skippingString: true)
    return open >>! close |>> { XMLTag.comment($0) }
}

private func endTag() -> Parser<XMLTag> {
    return character("/") >>! tagName() !>> character(">") |>> { XMLTag.end(name: $0) }
}

private func startOrSelfClosingTag(attributes: Parser<[String: String]>) -> Parser<XMLTag> {
    enum CloseType {
        case normalClosing, selfClosing
    }
    let normalClose = character(">") >>% CloseType.normalClosing
    let selfClose = string("/>", caseInsensitive: false, result: CloseType.selfClosing)
    let close = normalClose <|> selfClose
    
    let ws = skipWhitespaces()
    let pName = tagName()
    
    return pipe(pName, ws >>! attributes, close) { (name, attrs, closeType) in
        switch closeType {
        case .normalClosing:
            return XMLTag.start(name: name, attributes: attrs)
        case .selfClosing:
            return XMLTag.selfClosing(name: name, attributes: attrs)
        }
    }
}

private func tagName() -> Parser<String> {
    let isValidNameCharacter: (Character) -> Bool = { (c) in
        let punctuation: Set<Character> = ["/", ">", "="]
        return !Text.isWhitespaceChar(c) && !punctuation.contains(c)
    }
    return manyCharacters(minCount: 1, errorLabel: "letter", while: isValidNameCharacter)
}

private func attributes(attributeValue: Parser<String>,
                        entityDecoder: @escaping (String) -> String)
    -> Parser<[String: String]>
{
    let ws = skipWhitespaces()
    let ws1 = skipWhitespaces(atLeastOnce: true)
    let attrName = tagName()
    let equals = ws >>! character("=", result: ()) >>! ws
    let attr = attrName !>> equals !>>! attributeValue
    return many(attr, separator: ws1, atLeastOnce: false, allowEndBySeparator: true) |>> {
        var result: [String: String] = [:]
        for (name, value) in $0 {
            result[name] = entityDecoder(value)
        }
        return result
    }
}

private func xmlAttributeValue() -> Parser<String> {
    return surround(with: "\"") <|> surround(with: "'")
}

private func htmlAttributeValue() -> Parser<String> {
    let stringWithoutQuotes = manyCharacters(while: { !Text.isWhitespaceChar($0) && $0 != ">" })
    return xmlAttributeValue() <|> stringWithoutQuotes
}

private func surround(with quotationMark: Character) -> Parser<String> {
    let quotation = character(quotationMark, result: ())
    return between(open: quotation, close: quotation,
                   parser: manyCharacters(while: { $0 != quotationMark }))
}



