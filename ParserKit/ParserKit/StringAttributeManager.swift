
import Foundation

public struct StringAttributeManager {
    fileprivate var colorStack: [Color] = []
    fileprivate var boldCount: Int = 0
    fileprivate var italicCount: Int = 0
    fileprivate var underlineCount: Int = 0
    fileprivate var strikethroughCount: Int = 0
    fileprivate var superscriptCount: Int = 0
    
    public var attributes: [String: Any] {
        var result: [String: Any] = [:]
        
        if let color = colorStack.last {
            result[NSForegroundColorAttributeName] = color
        }
        
        // TODO: bold, italic 지원하기
        
        if underlineCount > 0 {
            result[NSUnderlineStyleAttributeName] = NSUnderlineStyle.styleSingle.rawValue
        }
        
        if strikethroughCount > 0 {
            result[NSStrikethroughStyleAttributeName] = NSUnderlineStyle.styleSingle.rawValue
        }
        
        if superscriptCount != 0 {
            result[NSSuperscriptAttributeName] = superscriptCount
        }
        
        return result
    }
    
    public mutating func handle(_ tag: XMLTag) {
        switch tag {
        case let .start(name, attributes):
            handleStartTag(name: name, attributes: attributes)
        case let .end(name):
            handleEndTag(name: name)
        case .selfClosing:
            break
        case .comment:
            break
        }
    }
    
    private mutating func handleStartTag(name: String, attributes: [String: String]) {
        guard let tagName = TagName(rawValue: name) else { return }
        switch tagName {
        case .font:
            if let colorStr = attributes["color"], let color = Color(webColorString: colorStr) {
                colorStack.append(color)
                break
            }
            // </font>가 나오면 무조건 top color를 지우기 때문에 color 속성이 없다면 이전 color를 넣어준다.
            // colorStack이 비어 있더라도 별다른 문제는 없다.
            if let topColor = colorStack.last {
                colorStack.append(topColor)
            }
        case .bold:
            boldCount += 1
        case .italic:
            italicCount += 1
        case .underline:
            underlineCount += 1
        case .strikethrough1, .strikethrough2:
            strikethroughCount += 1
        case .superscript:
            superscriptCount += 1
        case .`subscript`:
            superscriptCount -= 1
        }
    }
    
    private mutating func handleEndTag(name: String) {
        guard let tagName = TagName(rawValue: name) else { return }
        switch tagName {
        case .font:
            if !colorStack.isEmpty {
                colorStack.removeLast()
            }
        case .bold:
            boldCount -= 1
        case .italic:
            italicCount -= 1
        case .underline:
            underlineCount -= 1
        case .strikethrough1, .strikethrough2:
            strikethroughCount -= 1
        case .superscript:
            superscriptCount -= 1
        case .`subscript`:
            superscriptCount += 1
        }
    }
}

extension StringAttributeManager {
    fileprivate enum TagName: String {
        case font = "font"
        case bold = "b"
        case italic = "i"
        case underline = "u"
        case strikethrough1 = "strike"
        case strikethrough2 = "s"
        case superscript = "sup"
        case `subscript` = "sub"
    }
}



