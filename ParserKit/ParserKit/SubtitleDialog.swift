
import Foundation
import CoreMedia

open class SubtitleDialog {
    
}

open class SubtitleTextDialog: SubtitleDialog {
    @NSCopying open var text: NSAttributedString
//    open var text: NSAttributedString {
//        didSet { text = text.copy() as! NSAttributedString }
//    }
    
    public init(text: NSAttributedString = NSAttributedString()) {
        self.text = text.copy() as! NSAttributedString
    }
}

extension SubtitleFrame {
    public convenience init(text: NSAttributedString, timeRange: CMTimeRange) {
        let dialog = SubtitleTextDialog(text: text)
        self.init(dialogs: [dialog], timeRange: timeRange)
    }
    
    public convenience init(text: NSAttributedString, startTime: CMTime, duration: CMTime) {
        self.init(text: text, timeRange: CMTimeRange(start: startTime, duration: duration))
    }
    
    public convenience init(text: NSAttributedString, startTime: CMTime, endTime: CMTime) {
        self.init(text: text, startTime: startTime, duration: endTime - startTime)
    }
}



