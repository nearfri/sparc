
import Foundation
import CoreMedia

open class SubtitleFrame {
    open var dialogs: [SubtitleDialog]
    open var timeRange: CMTimeRange
    
    public init(dialogs: [SubtitleDialog] = [], timeRange: CMTimeRange = kCMTimeRangeZero) {
        self.dialogs = dialogs
        self.timeRange = timeRange
    }
    
    open var startTime: CMTime { return timeRange.start }
    open var endTime: CMTime { return timeRange.end }
    open var duration: CMTime { return timeRange.duration }
}



