
import SParc

public enum JSON {
    case string(String)
    case number(Double)
    case object([String: JSON])
    case array([JSON])
    case bool(Bool)
    case null
}

extension JSON: CustomStringConvertible {
    public var description: String {
        switch self {
        case let .string(str):
            return "\"" + str + "\""
        case let .number(num):
            return num.description
        case let .object(obj):
            return JSON.descriptionForObject(obj, indentLevel: 0)
        case let .array(arr):
            return JSON.descriptionForArray(arr, indentLevel: 0)
        case let .bool(boolean):
            return boolean.description
        case .null:
            return "null"
        }
    }
    
    private static let indentString = "    "
    
    private static func descriptionForObject(_ obj: [String: JSON], indentLevel: Int) -> String {
        if obj.count == 0 { return "{}" }
        var result = "{\n"
        for (index, element) in obj.enumerated() {
            if index != 0 { result += ",\n" }
            (0...indentLevel).forEach { _ in result += JSON.indentString }
            result += "\"" + element.0 + "\": "
            switch element.1 {
            case let .object(innerObj):
                result += JSON.descriptionForObject(innerObj, indentLevel: indentLevel + 1)
            case let .array(innerArr):
                result += JSON.descriptionForArray(innerArr, indentLevel: indentLevel + 1)
            default:
                result += element.1.description
            }
        }
        result += "\n"
        (0..<indentLevel).forEach { _ in result += JSON.indentString }
        result += "}"
        return result
    }
    
    private static func descriptionForArray(_ arr: [JSON], indentLevel: Int) -> String {
        if arr.count == 0 { return "[]" }
        let hasCollection = arr.contains {
            if case .object = $0 { return true }
            if case .array = $0 { return true }
            return false
        }
        if !hasCollection { return arr.description }
        
        let isAllObject = !arr.contains {
            if case .object = $0 { return false }
            return true
        }
        let indentLevelForObject = isAllObject ? indentLevel : indentLevel + 1
        var result = "["
        for (index, element) in arr.enumerated() {
            if case let .object(innerObj) = element {
                if index != 0 { result += ", " }
                result += JSON.descriptionForObject(innerObj, indentLevel: indentLevelForObject)
            } else {
                if index != 0 { result += ",\n" }
                else { result += "\n" }
                (0...indentLevel).forEach { _ in result += JSON.indentString }
                if case let .array(innerArr) = element {
                    result += JSON.descriptionForArray(innerArr, indentLevel: indentLevel + 1)
                } else {
                    result += element.description
                }
            }
        }
        if !isAllObject {
            result += "\n"
            (0..<indentLevel).forEach { _ in result += JSON.indentString }
        }
        result += "]"
        return result
    }
}

extension JSON: Equatable {}

public func ==(lhs: JSON, rhs: JSON) -> Bool {
    switch (lhs, rhs) {
    case let (.string(l), .string(r)):
        return l == r
    case let (.number(l), .number(r)):
        return l == r
    case let (.object(l), .object(r)):
        return l == r
    case let (.array(l), .array(r)):
        return l == r
    case let (.bool(l), .bool(r)):
        return l == r
    case (.null, .null):
        return true
    default:
        return false
    }
}

public func json() -> Parser<JSON> {
    let ws = skipWhitespaces()
    let ch = character
    let hex = hexadecimalDigit()
    let number = {
        return float([.allowMinusSign, .allowFractionalPart, .allowExponent])
    }()
    let stringLiteral: Parser<String> = {
        let escape: Parser<String> = any(of: "\"\\/bfnrt".characters) |>> {
            switch $0 {
            case "b": return "\u{0008}"
            case "f": return "\u{000C}"
            case "n": return "\n"
            case "r": return "\r"
            case "t": return "\t"
            default:  return String($0)
            }
        }
        let unicodeEscape: Parser<String> = ch("u") >>! pipe(hex, hex, hex, hex) {
            let scalar = Int(String([$0, $1, $2, $3]), radix: 16).flatMap({ UnicodeScalar($0) })
            return String(Character(scalar ?? UnicodeScalar(0)))
        }
        let normalChars: Parser<String> = manyCharacters(while: { $0 != "\"" && $0 != "\\" })
        let escapeChar: Parser<String> = ch("\\") >>! (escape <|> unicodeEscape)
        let text = manyStrings(normalChars, separator: escapeChar, includeSeparator: true)
        return between(open: ch("\""), close: ch("\""), parser: text)
    }()
    
    let jsonString = stringLiteral |>> { JSON.string($0) }
    let jsonNumber = number |>> { JSON.number($0) }
    let jsonTrue = string("true", result: JSON.bool(true))
    let jsonFalse = string("false", result: JSON.bool(false))
    let jsonNull = string("null", result: JSON.null)
    
    var innerJSONValue: Parser<JSON>! = nil
    let jsonValue = Parser { stream in innerJSONValue.parse(stream) }
    let pair: Parser<(String, JSON)> = tuple(stringLiteral, ws >>! ch(":") >>! ws >>! jsonValue)
    let jsonArray = between(open: ch("["), close: ch("]"),
                            parser: elements(jsonValue, { .array($0) }))
    let jsonObject = between(open: ch("{"), close: ch("}"), parser: elements(pair, {
        var objs: [String: JSON] = [:]
        $0.forEach { objs[$0] = $1 }
        return .object(objs)
    }))
    let parsers = [jsonObject, jsonArray, jsonString, jsonNumber, jsonTrue, jsonFalse, jsonNull]
    innerJSONValue = choice(parsers)
    return ws >>! jsonValue !>> ws !>> endOfStream()
}

// generic function 안에 generic function을 정의할 수 없어서 밖으로 뺐다.
private func elements<T>(_ element: Parser<T>, _ transform: @escaping ([T]) -> JSON)
    -> Parser<JSON>
{
    let ws: Parser<Void> = skipWhitespaces()
    return ws >>! many(element !>> ws, separator: string(",") !>> ws) |>> transform
}



