
import Foundation
import CoreMedia
import SParc

public final class SubRipParser {
    fileprivate let internalParser: Parser<Subtitle> = subRipSubtitle()
    
    public init() {
        
    }
    
    // 실패하면 에러를 throw 하는게 나으려나?
    public func parse(_ string: String) -> Subtitle? {
        switch internalParser.run(string) {
        case let .success(subtitle, _, _):
            return subtitle
        case let .failure(errors, _, _):
            print(errors)
            return nil
        }
    }
}

private func subRipSubtitle() -> Parser<Subtitle> {
    return subRipTrack() |>> {
        let result = Subtitle()
        result.format = "public.subrip-subtitle"
        result.tracks = [$0]
        return result
    }
}

private func subRipTrack() -> Parser<SubtitleTrack> {
    let ws = skipWhitespaces()
    let optionalFrames = ws >>! many(subRipFrame(), separator: ws, allowEndBySeparator: true)
    let frames: Parser<[SubtitleFrame]> = optionalFrames |>> {
        var result: [SubtitleFrame] = []
        for case let frame? in $0 {
            result.append(frame)
        }
        return result
    }
    
    return frames |>> {
        let result = SubtitleTrack()
        for frame in $0 {
            result.insertFrame(frame)
        }
        return result
    }
}

private func subRipFrame() -> Parser<SubtitleFrame?> {
    let frame = pipe(frameIndex(), timeRange(), textSection()) { (index, range, text) in
        return SubtitleFrame(text: text, timeRange: range)
    }
    
    return Parser { (stream) in
        let state = stream.state
        switch frame.parse(stream) {
        case let .success(v, e):
            return .success(v, e)
        case let .failure(e), let .fatalFailure(e):
            // 중간에 실패하면 다음 라인으로 넘어가서 계속 파싱할 수 있도록 한다
            if stream.skipRestOfLine().count > 0 {
                return .success(nil, e)
            }
            // 파일 끝이면 subRipTrack()의 many()가 성공할 수 있도록 backtrack 한다
            assert(stream.isAtEndOfStream)
            stream.backtrack(to: state)
            return .failure(e)
        }
    }
}

private func frameIndex() -> Parser<Int> {
    return integer() !>> skipRestOfLine()
}

private func timeRange() -> Parser<CMTimeRange> {
    func digit(figures: Int) -> Parser<Double> {
        return manyCharacters(minCount: figures, maxCount: figures,
                              errorLabel: "number", while: isDecimalDigit)
            |>> { Double($0)! }
    }
    
    let timeGenerator = { (hours: Double, mins: Double, secs: Double, ms: Double) in
        return CMTime(seconds: hours * 3600 + mins * 60 + secs + ms * 0.001,
                      preferredTimescale: 1_000_000_000)
    }
    
    let time = pipe(digit(figures: 2),
                    skipCharacter(":") >>! digit(figures: 2),
                    skipCharacter(":") >>! digit(figures: 2),
                    skipCharacter(",") >>! digit(figures: 3),
                    timeGenerator)
    
    let skipBlankChars = skipManyCharacters(minCount: 1, while: Text.isBlankChar)
    let arrow = skipBlankChars >>! skipString("-->") >>! skipBlankChars
    
    return time !>> arrow !>>! time !>> skipRestOfLine() |>> { (start, end) in
        return CMTimeRange(start: start, end: end)
    }
}

private func textSection() -> Parser<NSAttributedString> {
    enum Node {
        case tag(XMLTag)
        case text(String)
    }
    
    let pTag = htmlTag() |>> { Node.tag($0) }
    
    let pText = manyCharacters(minCount: 1, errorLabel: "text", while: { (c) -> Bool in
        return !Text.isNewlineChar(c) && c != "<"
    }) |>> { Node.text($0) }
    
    let singleNewline = skipNewline() >>!? followed(by: { !Text.isNewlineChar($0) })
        |>> { Node.text("\n") }
    
    let nodes = many(first: choice([pTag, pText]), while: choice([pTag, pText, singleNewline]))
    
    return nodes |>> {
        let result = NSMutableAttributedString()
        result.beginEditing()
        var attributeManager = StringAttributeManager()
        for node in $0 {
            switch node {
            case .tag(let tag):
                attributeManager.handle(tag)
            case .text(let text):
                let attributes = attributeManager.attributes
                let attrString = NSAttributedString(string: text, attributes: attributes)
                result.append(attrString)
            }
        }
        result.endEditing()
        return result
    }
}



