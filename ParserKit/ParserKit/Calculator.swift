
import SParc
import Foundation

public func calculator() -> Parser<Double> {
    let tokenizer = calculatorTokenizer()
    let prattParser = calculatorPrattParser()
    
    return Parser { (stream) in
        return prattParser.parse(stream, with: tokenizer)
    }
}

internal func calculatorTokenizer() -> Parser<Token> {
    let ws = skipWhitespaces()
    
    let end = endOfStream() |>> { Token(type: .end) }
    
    let numberOptions: NumberLiteral.Options = [
        .allowFractionalPart, .allowNoIntegerPart, .allowExponent
    ]
    let number = numberLiteral(numberOptions) !>> ws |>> { Token(type: .number, value: $0.string) }
    
    let singleOperator = any(of: "!^*/%+-(),".characters)
        |>> { Token(type: .operator, value: String($0)) }
    
    let name: Parser<Token> = {
        let firstIdentifierChar = { (c: Character) -> Bool in
            return isASCIILetter(c) || c == "_"
        }
        let identifierChar = { (c: Character) -> Bool in
            return isASCIILetter(c) || isDecimalDigit(c) || c == "_"
        }
        return manyCharacters(minCount: 1, first: firstIdentifierChar, while: identifierChar)
            |>> { Token(type: .name, value: $0) }
    }()
    
    return ws >>! choice([number, name, singleOperator, end])
}

internal func calculatorPrattParser() -> PrattParser<Double> {
    let prattParser: PrattParser<Double> = PrattParser()
    addNormalOperators(to: prattParser)
    addFunctionOperators(to: prattParser)
    return prattParser
}

private func addNormalOperators(to prattParser: PrattParser<Double>) {
    func addOperator(_ value: String, exp: @escaping NullDenotation<Double>.Expression) {
        let token = Token(type: .operator, value: value)
        prattParser.addDenotation(for: token, expression: exp)
    }
    
    func addOperator(_ value: String, bp: Int, exp: @escaping LeftDenotation<Double>.Expression) {
        let token = Token(type: .operator, value: value)
        prattParser.addDenotation(for: token, bindingPower: bp, expression: exp)
    }
    
    prattParser.addDenotation(for: .end, bindingPower: 0) { (left, _, _) -> Double in
        return left
    }
    prattParser.addDenotation(for: .number) { (token, parser) -> Double in
        guard let number = Double(token.value) else {
            throw ParserError.Other(message: "\(token.value) is outside the allowable range")
        }
        return number
    }
    addOperator("^", bp: 140) { (left, token, parser) -> Double in
        let right = try parser.expression(withRightBindingPower: 139)
        return pow(left, right)
    }
    addOperator("+") { (token, parser) -> Double in
        return try parser.expression(withRightBindingPower: 130)
    }
    addOperator("-") { (token, parser) -> Double in
        return try -parser.expression(withRightBindingPower: 130)
    }
    addOperator("*", bp: 120) { (left, token, parser) -> Double in
        return try left * parser.expression(withRightBindingPower: 120)
    }
    addOperator("/", bp: 120) { (left, token, parser) -> Double in
        return try left / parser.expression(withRightBindingPower: 120)
    }
    addOperator("%", bp: 120) { (left, token, parser) -> Double in
        let right = try parser.expression(withRightBindingPower: 120)
        return left.truncatingRemainder(dividingBy: right)
    }
    addOperator("+", bp: 110) { (left, token, parser) -> Double in
        return try left + parser.expression(withRightBindingPower: 110)
    }
    addOperator("-", bp: 110) { (left, token, parser) -> Double in
        return try left - parser.expression(withRightBindingPower: 110)
    }
    addOperator(")", bp: 0) { (left, token, parser) -> Double in
        return left
    }
    addOperator("(") { (token, parser) -> Double in
        let expr = try parser.expression(withRightBindingPower: 0)
        guard parser.nextToken.value == ")" else {
            throw ParserError.ExpectedString(")")
        }
        try parser.advance()
        return expr
    }
}

private func addFunctionOperators(to prattParser: PrattParser<Double>) {
    func addOperator(_ value: String, bp: Int, exp: @escaping LeftDenotation<Double>.Expression) {
        let token = Token(type: .operator, value: value)
        prattParser.addDenotation(for: token, bindingPower: bp, expression: exp)
    }
    
    addOperator(",", bp: 0) { (left, token, parser) -> Double in
        return left
    }
    
    func addFunction(_ value: String, compute: @escaping ([Double]) throws -> Double) {
        let token = Token(type: .name, value: value)
        prattParser.addDenotation(for: token) { (token, parser) -> Double in
            guard parser.nextToken.value == "(" else {
                throw ParserError.ExpectedString("(")
            }
            try parser.advance()
            
            var params: [Double] = []
            if parser.nextToken.value != ")" {
                while true {
                    let param = try parser.expression(withRightBindingPower: 0)
                    params.append(param)
                    guard parser.nextToken.value == "," else {
                        break
                    }
                    try parser.advance()
                }
            }
            let expr = try compute(params)
            
            guard parser.nextToken.value == ")" else {
                throw ParserError.ExpectedString(")")
            }
            try parser.advance()
            
            return expr
        }
    }
    
    func addFunction1(_ value: String, compute: @escaping (Double) throws -> Double) {
        addFunction(value) { (params) -> Double in
            guard params.count == 1 else {
                throw ParserError.Other(message: "\"\(value)\" function take exactly one argument")
            }
            return try compute(params[0])
        }
    }
    
    func addFunction2(_ value: String, compute: @escaping (Double, Double) throws -> Double) {
        addFunction(value) { (params) -> Double in
            guard params.count == 2 else {
                throw ParserError.Other(message: "\"\(value)\" function take exactly two argument")
            }
            return try compute(params[0], params[1])
        }
    }
    
    addFunction1("sin", compute: sin)
    addFunction1("cos", compute: cos)
    addFunction1("tan", compute: tan)
    addFunction1("exp", compute: exp)
    addFunction1("log", compute: log)
    addFunction1("sqrt", compute: sqrt)
    addFunction2("pow", compute: pow)
}



