
import SParc

public func javascriptTokenizer() -> Parser<Token> {
    let ws = skipWhitespaces()
    let ch = character
    let hex = hexadecimalDigit()
    
    let end = endOfStream() |>> { Token(type: .end) }
    
    let numberOptions: NumberLiteral.Options = [
        .allowMinusSign, .allowFractionalPart, .allowExponent
    ]
    let number = numberLiteral(numberOptions) |>> { Token(type: .number, value: $0.string) }
    
    let escape: Parser<String> = any(of: "\"'\\bfnrt".characters) |>> {
        switch $0 {
        case "b": return "\u{0008}"
        case "f": return "\u{000C}"
        case "n": return "\n"
        case "r": return "\r"
        case "t": return "\t"
        default:  return String($0)
        }
    }
    let unicodeEscape: Parser<String> = ch("u") >>! pipe(hex, hex, hex, hex) {
        let scalar = Int(String([$0, $1, $2, $3]), radix: 16).flatMap({ UnicodeScalar($0) })
        return String(Character(scalar ?? UnicodeScalar(0)))
    }
    let escapeChar: Parser<String> = ch("\\") >>! (escape <|> unicodeEscape)
    
    let stringLiteral1: Parser<Token> = {
        let normalChars = manyCharacters(while: { $0 != "\"" && $0 != "\\" })
        let text = manyStrings(normalChars, separator: escapeChar, includeSeparator: true)
        return between(open: ch("\""), close: ch("\""), parser: text)
            |>> { Token(type: .string, value: $0) }
    }()
    
    let stringLiteral2: Parser<Token> = {
        let normalChars = manyCharacters(while: { $0 != "'" && $0 != "\\" })
        let text = manyStrings(normalChars, separator: escapeChar, includeSeparator: true)
        return between(open: ch("'"), close: ch("'"), parser: text)
            |>> { Token(type: .string, value: $0) }
    }()
    
    let multiOperator: Parser<Token> =
        manyCharacters(minCount: 2,
                       first: { "=<>!+-*&|/%^".characters.contains($0) },
                       while: { "=<>&|".characters.contains($0) })
            |>> { Token(type: .operator, value: $0) }
    
    let singleOperator = any(of: "~!@#$%^&*()-+=[]{}\\|;:,.<>/?".characters)
        |>> { Token(type: .operator, value: String($0)) }
    
    let name: Parser<Token> = {
        let firstIdentifierChar = { (c: Character) -> Bool in
            return isASCIILetter(c) || c == "_"
        }
        let identifierChar = { (c: Character) -> Bool in
            return isASCIILetter(c) || isDecimalDigit(c) || c == "_"
        }
        return manyCharacters(minCount: 1, first: firstIdentifierChar, while: identifierChar)
            |>> { Token(type: .name, value: $0) }
    }()
    
    let comment: Parser<Token> = skipString("//") >>! ws >>! restOfLine()
        |>> { Token(type: .comment, value: $0) }
    
    return ws >>! choice([comment, name, number, multiOperator, singleOperator,
                          stringLiteral1, stringLiteral2, end])
}



