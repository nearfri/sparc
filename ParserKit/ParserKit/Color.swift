
import Foundation
#if os(macOS)
    import AppKit
    public typealias Color = NSColor
#else
    import UIKit
    public typealias Color = UIColor
#endif

extension Color {
    internal convenience init?(webColorString: String) {
        guard let rgb = rgbFromWebColorString(webColorString) else { return nil }
        let r = CGFloat((rgb & 0xFF0000) >> 16)
        let g = CGFloat((rgb & 0x00FF00) >> 8)
        let b = CGFloat((rgb & 0x0000FF) >> 0)
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1.0)
    }
}

private func rgbFromWebColorString(_ string: String) -> Int? {
    guard string.characters.count > 1 else { return nil }
    
    if string.hasPrefix("#") {
        return Int(String(string.characters.dropFirst()), radix: 16)
    }
    
    return webColors[string.lowercased()]
}

private let webColors: [String: Int] = [
    // HTML color names
    "white":                0xFFFFFF, // White color
    "silver":               0xC0C0C0, // Gray color
    "gray":                 0x808080, // Gray color
    "black":                0x000000, // Gray color
    "red":                  0xFF0000, // Red color
    "maroon":               0x800000, // Brown color
    "yellow":               0xFFFF00, // Yellow color
    "olive":                0x808000, // Green color
    "lime":                 0x00FF00, // Green color
    "green":                0x008000, // Green color
    "aqua":                 0x00FFFF, // Blue/Cyan color
    "teal":                 0x008080, // Green color
    "blue":                 0x0000FF, // Blue/Cyan color
    "navy":                 0x000080, // Blue/Cyan color
    "fuchsia":              0xFF00FF, // Purple color
    "purple":               0x800080, // Purple color
    
    // X11 color names
    // Red colors
    "indianred":            0xCD5C5C,
    "lightcoral":           0xF08080,
    "salmon":               0xFA8072,
    "darksalmon":           0xE9967A,
    "lightsalmon":          0xFFA07A,
//    "red":                  0xFF0000,
    "crimson":              0xDC143C,
    "firebrick":            0xB22222,
    "darkred":              0x8B0000,
    
    // Pink colors
    "pink":                 0xFFC0CB,
    "lightpink":            0xFFB6C1,
    "hotpink":              0xFF69B4,
    "deeppink":             0xFF1493,
    "mediumvioletred":      0xC71585,
    "palevioletred":        0xDB7093,
    
    // Orange colors
//    "lightsalmon":          0xFFA07A,
    "coral":                0xFF7F50,
    "tomato":               0xFF6347,
    "orangered":            0xFF4500,
    "darkorange":           0xFF8C00,
    "orange":               0xFFA500,
    
    // Yellow colors
    "gold":                 0xFFD700,
//    "yellow":               0xFFFF00,
    "lightyellow":          0xFFFFE0,
    "lemonchiffon":         0xFFFACD,
    "lightgoldenrodyellow": 0xFAFAD2,
    "papayawhip":           0xFFEFD5,
    "moccasin":             0xFFE4B5,
    "peachpuff":            0xFFDAB9,
    "palegoldenrod":        0xEEE8AA,
    "khaki":                0xF0E68C,
    "darkkhaki":            0xBDB76B,
    
    // Purple colors
    "lavender":             0xE6E6FA,
    "thistle":              0xD8BFD8,
    "plum":                 0xDDA0DD,
    "violet":               0xEE82EE,
    "orchid":               0xDA70D6,
//    "fuchsia":              0xFF00FF,
    "magenta":              0xFF00FF,
    "mediumorchid":         0xBA55D3,
    "mediumpurple":         0x9370DB,
    "blueviolet":           0x8A2BE2,
    "darkviolet":           0x9400D3,
    "darkorchid":           0x9932CC,
    "darkmagenta":          0x8B008B,
//    "purple":               0x800080,
    "indigo":               0x4B0082,
    "darkslateblue":        0x483D8B,
    "slateblue":            0x6A5ACD,
    "mediumslateblue":      0x7B68EE,
    
    // Green colors
    "greenyellow":          0xADFF2F,
    "chartreuse":           0x7FFF00,
    "lawngreen":            0x7CFC00,
//    "lime":                 0x00FF00,
    "limegreen":            0x32CD32,
    "palegreen":            0x98FB98,
    "lightgreen":           0x90EE90,
    "mediumspringgreen":    0x00FA9A,
    "springgreen":          0x00FF7F,
    "mediumseagreen":       0x3CB371,
    "seagreen":             0x2E8B57,
    "forestgreen":          0x228B22,
//    "green":                0x008000,
    "darkgreen":            0x006400,
    "yellowgreen":          0x9ACD32,
    "olivedrab":            0x6B8E23,
//    "olive":                0x808000,
    "darkolivegreen":       0x556B2F,
    "mediumaquamarine":     0x66CDAA,
    "darkseagreen":         0x8FBC8F,
    "lightseagreen":        0x20B2AA,
    "darkcyan":             0x008B8B,
//    "teal":                 0x008080,
    
    // Blue/Cyan colors
//    "aqua":                 0x00FFFF,
    "cyan":                 0x00FFFF,
    "lightcyan":            0xE0FFFF,
    "paleturquoise":        0xAFEEEE,
    "aquamarine":           0x7FFFD4,
    "turquoise":            0x40E0D0,
    "mediumturquoise":      0x48D1CC,
    "darkturquoise":        0x00CED1,
    "cadetblue":            0x5F9EA0,
    "steelblue":            0x4682B4,
    "lightsteelblue":       0xB0C4DE,
    "powderblue":           0xB0E0E6,
    "lightblue":            0xADD8E6,
    "skyblue":              0x87CEEB,
    "lightskyblue":         0x87CEFA,
    "deepskyblue":          0x00BFFF,
    "dodgerblue":           0x1E90FF,
    "cornflowerblue":       0x6495ED,
    "royalblue":            0x4169E1,
//    "blue":                 0x0000FF,
    "mediumblue":           0x0000CD,
    "darkblue":             0x00008B,
//    "navy":                 0x000080,
    "midnightblue":         0x191970,
    
    // Brown colors
    "cornsilk":             0xFFF8DC,
    "blanchedalmond":       0xFFEBCD,
    "bisque":               0xFFE4C4,
    "navajowhite":          0xFFDEAD,
    "wheat":                0xF5DEB3,
    "burlywood":            0xDEB887,
    "tan":                  0xD2B48C,
    "rosybrown":            0xBC8F8F,
    "sandybrown":           0xF4A460,
    "goldenrod":            0xDAA520,
    "darkgoldenrod":        0xB8860B,
    "peru":                 0xCD853F,
    "chocolate":            0xD2691E,
    "saddlebrown":          0x8B4513,
    "sienna":               0xA0522D,
    "brown":                0xA52A2A,
//    "maroon":               0x800000,
    
    // White colors
//    "white":                0xFFFFFF,
    "snow":                 0xFFFAFA,
    "honeydew":             0xF0FFF0,
    "mintcream":            0xF5FFFA,
    "azure":                0xF0FFFF,
    "aliceblue":            0xF0F8FF,
    "ghostwhite":           0xF8F8FF,
    "whitesmoke":           0xF5F5F5,
    "seashell":             0xFFF5EE,
    "beige":                0xF5F5DC,
    "oldlace":              0xFDF5E6,
    "floralwhite":          0xFFFAF0,
    "ivory":                0xFFFFF0,
    "antiquewhite":         0xFAEBD7,
    "linen":                0xFAF0E6,
    "lavenderblush":        0xFFF0F5,
    "mistyrose":            0xFFE4E1,
    
    // Gray colors
    "gainsboro":            0xDCDCDC,
    "lightgrey":            0xD3D3D3,
//    "silver":               0xC0C0C0,
    "darkgray":             0xA9A9A9,
//    "gray":                 0x808080,
    "dimgray":              0x696969,
    "lightslategray":       0x778899,
    "slategray":            0x708090,
    "darkslategray":        0x2F4F4F,
//    "black":                0x000000,
]



