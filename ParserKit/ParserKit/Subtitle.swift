
import Foundation
import CoreMedia

open class Subtitle {
    open var title: String = ""
    open var format: String = ""
    open var url: URL? = nil
    open var tracks: [SubtitleTrack] = []
    
    open var timeRange: CMTimeRange {
        guard let firstTrack = tracks.min(by: { $0.timeRange.start < $1.timeRange.start }),
            let lastTrack = tracks.max(by: { $0.timeRange.end < $1.timeRange.end })
            else { return kCMTimeRangeZero }
        return CMTimeRange(start: firstTrack.timeRange.start, end: lastTrack.timeRange.end)
    }
    
    open func track(withTrackID trackID: CMPersistentTrackID) -> SubtitleTrack? {
        return tracks.first { $0.trackID == trackID }
    }
    
    open func removeTrack(withTrackID trackID: CMPersistentTrackID) {
        if let index = tracks.index(where: { $0.trackID == trackID }) {
            tracks.remove(at: index)
        }
    }
}



