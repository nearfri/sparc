
import XCTest
@testable import SParc

class CharacterStreamTests: XCTestCase {
    typealias Index = CharacterStream.Index

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testStreamConstructors() {
        let str = "1234567890"
        let regex = try! NSRegularExpression(pattern: ".*", options: [])
        
        func checkStream(_ stream: CharacterStream, startIndex: Index, count: Int)
        {
            let endIndex = stream.string.index(startIndex, offsetBy: count)
            XCTAssertEqual(stream.startIndex, startIndex)
            XCTAssertEqual(stream.endIndex, endIndex)
            XCTAssertEqual(stream.nextIndex, startIndex)
            XCTAssertEqual(stream.lineStartIndex, startIndex)
            XCTAssertEqual(stream.lineNumber, 1)
            
            if count == 0 {
                XCTAssertNil(stream.peek())
                XCTAssertTrue(stream.isAtEndOfStream)
            } else {
                XCTAssertEqual(stream.peek(), str[startIndex])
                XCTAssertTrue(stream.skip(str[startIndex..<endIndex]))
                XCTAssertEqual(stream.nextIndex, endIndex)
                XCTAssertTrue(stream.isAtEndOfStream)
                stream.seek(to: startIndex)
                XCTAssertEqual(stream.nextIndex, startIndex)
                let matchRange = stream.match(regex)?.range
                XCTAssertNotNil(matchRange)
                if let matchRange = matchRange {
                    XCTAssertEqual(
                        (stream.string as NSString).substring(with: matchRange),
                        str[startIndex..<endIndex])
                }
            }
        }
        
        var startIndex = str.startIndex
        var stream = CharacterStream(string: str)
        checkStream(stream, startIndex: startIndex, count: str.characters.count)
        
        stream = CharacterStream(string: str, startIndex: startIndex, endIndex: str.endIndex)
        checkStream(stream, startIndex: startIndex, count: str.characters.count)
        
        startIndex = str.characters.index(str.startIndex, offsetBy: 1)
        stream = CharacterStream(string: str, startIndex: startIndex, endIndex: str.endIndex)
        checkStream(stream, startIndex: startIndex, count: str.characters.count - 1)

        stream = CharacterStream(string: str, startIndex: startIndex, endIndex: str.index(startIndex, offsetBy: 1))
        checkStream(stream, startIndex: startIndex, count: 1)
        
        stream = CharacterStream(string: str, startIndex: startIndex, endIndex: startIndex)
        checkStream(stream, startIndex: startIndex, count: 0)
    }
    
    func checkEmptyStream(_ stream: CharacterStream) {
        let index0 = stream.nextIndex
        
        func checkNextIndex(_ line: UInt = #line) {
            XCTAssertEqual(stream.nextIndex, index0, "line: \(line)")
        }
        
        func checkIndex() {
            XCTAssertTrue(stream.isAtStartOfStream)
            XCTAssertTrue(stream.isAtEndOfStream)
            XCTAssertEqual(stream.startIndex, index0)
            XCTAssertEqual(stream.endIndex, index0)
            XCTAssertEqual(stream.endIndex, stream.string.endIndex)
        }
        checkIndex()
        
        func checkPeek() {
            XCTAssertNil(stream.peek())
            XCTAssertNil(stream.peek(offset: 0))
            XCTAssertNil(stream.peek(offset: 1))
            XCTAssertNil(stream.peek(offset: -1))
            XCTAssertNil(stream.peek(offset: Int.max))
            XCTAssertNil(stream.peek(offset: Int.min))
            XCTAssertEqual(stream.peek(count: 0), "")
            XCTAssertEqual(stream.peek(count: 1), "")
            XCTAssertEqual(stream.peek(count: Int.max), "")
        }
        checkPeek()
        
        func checkRead() {
            XCTAssertNil(stream.read())
            checkNextIndex()
            XCTAssertEqual(stream.read(0), "")
            checkNextIndex()
            XCTAssertEqual(stream.read(1), "")
            checkNextIndex()
            XCTAssertEqual(stream.read(Int.max), "")
            checkNextIndex()
            
            let state = stream.state
            XCTAssertEqual(stream.read(from: state), "")
        }
        checkRead()
        
        func checkMatch() {
            XCTAssertTrue(stream.match(""))
            XCTAssertFalse(stream.match("x"))
            XCTAssertNil(stream.match(try! NSRegularExpression(pattern: "x", options: [])))
        }
        checkMatch()
        
        func checkSkip() {
            XCTAssertTrue(stream.skip(""))
            checkNextIndex()
            XCTAssertFalse(stream.skip("x"))
            checkNextIndex()
            XCTAssertTrue(stream.skip(""))
            checkNextIndex()
            XCTAssertFalse(stream.skip("x"))
            checkNextIndex()
            
            let tag = stream.stateTag
            stream.skip()
            checkNextIndex()
            XCTAssertEqual(stream.stateTag, tag)
            stream.skip(0)
            checkNextIndex()
            stream.skip(1)
            checkNextIndex()
            stream.skip(Int.max)
            checkNextIndex()
            
            XCTAssertNil(stream.skipAndPeek(offset: 0))
            checkNextIndex()
            XCTAssertNil(stream.skipAndPeek(offset: 1))
            checkNextIndex()
            XCTAssertNil(stream.skipAndPeek(offset: Int.max))
            checkNextIndex()
            XCTAssertNil(stream.skipAndPeek(offset: -1))
            checkNextIndex()
            XCTAssertNil(stream.skipAndPeek(offset: Int.min))
            checkNextIndex()
        }
        checkSkip()
        
        let tag = stream.stateTag
        
        func checkSkipSubstring() {
            XCTAssertFalse(stream.skipWhitespaces())
            checkNextIndex()
            XCTAssertFalse(stream.skipNewline())
            checkNextIndex()
            
            XCTAssertEqual(stream.skipNewlineThenWhitespaces(tabWidth: 8), -1)
            checkNextIndex()
            
            stream.skipRestOfLine(thenSkippingNewline: false)
            checkNextIndex()
            stream.skipRestOfLine(thenSkippingNewline: true)
            checkNextIndex()
            
            XCTAssertEqual(stream.skipSubstring(maxCount: 1).count, 0)
            checkNextIndex()
            XCTAssertEqual(stream.skipSubstring(maxCount: Int.max).count, 0)
            checkNextIndex()
            
            XCTAssertEqual(stream.skipSubstring(while: { _ in true }).count, 0)
            checkNextIndex()
            XCTAssertEqual(stream.skipSubstring(minCount: 0, maxCount: 1, while: { _ in true }).count, 0)
            checkNextIndex()
            XCTAssertEqual(stream.skipSubstring(minCount: 0, maxCount: Int.max, while: { _ in true }).count, 0)
            checkNextIndex()
            
            XCTAssertEqual(stream.skipSubstring(until: "1", maxCount: 1).bounds.count, 0)
            checkNextIndex()
            XCTAssertEqual(stream.skipSubstring(until: "1", maxCount: Int.max).bounds.count, 0)
            checkNextIndex()
        }
        checkSkipSubstring()
        
        func checkReadSubstring() {
            XCTAssertEqual(stream.readRestOfLine(thenSkippingNewline: false), "")
            checkNextIndex()
            XCTAssertEqual(stream.readRestOfLine(thenSkippingNewline: true), "")
            
            XCTAssertNil(stream.readCharacter())
            
            XCTAssertEqual(stream.readSubstring(maxCount: 1, normalizingNewlines: false), "")
            checkNextIndex()
            XCTAssertEqual(stream.readSubstring(maxCount: 1, normalizingNewlines: true), "")
            checkNextIndex()
            XCTAssertEqual(stream.readSubstring(maxCount: Int.max, normalizingNewlines: false), "")
            checkNextIndex()
            XCTAssertEqual(stream.readSubstring(maxCount: Int.max, normalizingNewlines: true), "")
            checkNextIndex()
            
            XCTAssertEqual(stream.readSubstring(minCount: 0, maxCount: 1, normalizingNewlines: false, while: { _ in true }), "")
            checkNextIndex()
            XCTAssertEqual(stream.readSubstring(minCount: 0, maxCount: 1, normalizingNewlines: true, while: { _ in true }), "")
            checkNextIndex()
            XCTAssertEqual(stream.readSubstring(minCount: 0, maxCount: Int.max, normalizingNewlines: false, while: { _ in true }), "")
            checkNextIndex()
            XCTAssertEqual(stream.readSubstring(minCount: 0, maxCount: Int.max, normalizingNewlines: true, while: { _ in true }), "")
            checkNextIndex()
            
            XCTAssertEqual(stream.readSubstring(until: "1", maxCount: 1, normalizingNewlines: false).bounds.count, 0)
            checkNextIndex()
            XCTAssertEqual(stream.readSubstring(until: "1", maxCount: Int.max, normalizingNewlines: false).bounds.count, 0)
            checkNextIndex()
        }
        checkReadSubstring()
        
        XCTAssertEqual(stream.stateTag, tag)
    }
    
    func checkBasicCharacterStreamMethods(_ stream: CharacterStream, srcString: String) {
        let index0 = stream.startIndex
        let state0 = stream.state
        let srcCount = srcString.characters.count
        let dollarStr = String(repeating: "$", count: srcCount)
        
        func checkProperties() {
            stream.backtrack(to: state0)
            let tag = stream.stateTag
            
            stream.name = "Name2"
            XCTAssertEqual(stream.name, "Name2")
            XCTAssertEqual(stream.stateTag, tag + 1)
            
            stream.skip()
            XCTAssertEqual(stream.columnNumber, 1 + stream.string.distance(from: stream.lineStartIndex, to: stream.nextIndex))
            
            stream.stateTag = tag
            XCTAssertEqual(stream.stateTag, tag)
        }
        checkProperties()
        
        func checkRegisterNewlines() {
            stream.backtrack(to: state0)
            let line0 = stream.lineNumber
            let tag0 = stream.stateTag
            
            stream.skip()
            stream.registerNewline()
            XCTAssertEqual(stream.stateTag, tag0 + 1 + 1)
            XCTAssertEqual(stream.lineNumber, line0 + 1)
            XCTAssertEqual(stream.lineStartIndex, stream.nextIndex)
            
            stream.backtrack(to: state0)
            stream.skip(3)
            stream.registerNewlines(lineCount: 2, columnOffset: 1)
            XCTAssertEqual(stream.stateTag, tag0 + 1 + 1)
            XCTAssertEqual(stream.lineNumber, line0 + 2)
            XCTAssertEqual(stream.lineStartIndex, stream.string.index(before: stream.nextIndex))
        }
        checkRegisterNewlines()
        
        func seek(to offset: Int) {
            stream.backtrack(to: state0)
            stream.skip(offset)
        }
        
        func checkMove(offset1: Int, offset2: Int) {
            var tag1 = state0.tag
            let index1 = stream.string.index(index0, offsetBy: min(offset1, srcCount))
            let index2 = stream.string.index(index0, offsetBy: min(offset2, srcCount))
            let c1: Character? = offset1 < srcCount ? srcString[srcString.characters.index(srcString.startIndex, offsetBy: offset1)] : nil
            let c2: Character? = offset2 < srcCount ? srcString[srcString.characters.index(srcString.startIndex, offsetBy: offset2)] : nil
            let d = offset2 - min(offset1, srcCount)
            
            stream.backtrack(to: state0)
            stream.seek(to: stream.string.index(index0, offsetBy: offset1, limitedBy: stream.string.endIndex) ?? stream.string.endIndex)
            XCTAssertEqual(stream.nextIndex, index1)
            let indexToken1 = stream.indexToken
            if indexToken1.index != state0.index {
                tag1 += 1
                XCTAssertEqual(stream.stateTag, tag1)
            }
            XCTAssertEqual(stream.peek(), c1)
            XCTAssertEqual(stream.isAtStartOfStream, offset1 == 0)
            XCTAssertEqual(stream.isAtEndOfStream, offset1 >= srcCount)
            
            var tag2 = tag1
            stream.seek(to: stream.string.index(index0, offsetBy: offset2, limitedBy: stream.string.endIndex) ?? stream.string.endIndex)
            XCTAssertEqual(stream.nextIndex, index2)
            if stream.nextIndex != indexToken1.index {
                tag2 += 1
                XCTAssertEqual(stream.stateTag, tag2)
            }
            XCTAssertEqual(stream.peek(), c2)
            XCTAssertEqual(stream.isAtStartOfStream, offset2 == 0)
            XCTAssertEqual(stream.isAtEndOfStream, offset2 >= srcCount)
            
            XCTAssertEqual(indexToken1.index(in: stream), index1)
            stream.seek(to: indexToken1)
            XCTAssertEqual(stream.nextIndex, index1)
            XCTAssertEqual(stream.peek(), c1)
            
            seek(to: offset1)
            XCTAssertEqual(stream.peek(offset: d), c2)
            XCTAssertEqual(stream.nextIndex, index1)
            XCTAssertEqual(stream.stateTag, tag1)
            
            func checkStream(_ line: UInt = #line) {
                XCTAssertEqual(stream.nextIndex, index2, "line: \(line)")
                XCTAssertEqual(stream.stateTag, tag2, "line: \(line)")
            }
            
            seek(to: offset1)
            stream.skip(d)
            checkStream()
            
            seek(to: offset1)
            XCTAssertEqual(stream.skipAndPeek(offset: d), c2)
            checkStream()
            
            if d >= 0 {
                if d == 1 {
                    seek(to: offset1)
                    stream.skip()
                    XCTAssertEqual(stream.nextIndex, index2)
                    if index2 == index1 { XCTAssertEqual(stream.stateTag, tag1) }
                    else { XCTAssertEqual(stream.stateTag, tag2) }
                    
                    seek(to: offset1)
                    XCTAssertEqual(stream.skipAndPeek(), c2)
                    XCTAssertEqual(stream.nextIndex, index2)
                    if index2 == index1 { XCTAssertEqual(stream.stateTag, tag1) }
                    else { XCTAssertEqual(stream.stateTag, tag2) }
                }
                seek(to: offset1)
                stream.skip(d)
                checkStream()
                
                seek(to: offset1)
                XCTAssertEqual(stream.skipAndPeek(offset: d), c2)
                checkStream()
            } else if offset2 == 0 {
                seek(to: offset1)
                XCTAssertEqual(stream.peek(offset: d - 1), nil)
                XCTAssertEqual(stream.nextIndex, index1)
                XCTAssertEqual(stream.stateTag, tag1)
                XCTAssertEqual(stream.peek(offset: Int.min), nil)
                XCTAssertEqual(stream.nextIndex, index1)
                XCTAssertEqual(stream.stateTag, tag1)
                
                seek(to: offset1)
                XCTAssertEqual(stream.skipAndPeek(offset: d - 1), nil)
                checkStream()
                
                seek(to: offset1)
                XCTAssertEqual(stream.skipAndPeek(offset: Int.min), nil)
                checkStream()
            }
        }
        for offset1 in 0..<srcCount+2 {
            for offset2 in 0..<srcCount+2 {
                checkMove(offset1: offset1, offset2: offset2)
            }
        }
        
        func checkMoveException() {
            let endIndex = stream.string.index(index0, offsetBy: srcCount)
            stream.seek(to: endIndex)
            XCTAssertEqual(stream.nextIndex, endIndex)
            XCTAssertTrue(stream.isAtEndOfStream)
            
            for i in 0..<srcCount {
                seek(to: i)
                stream.skip(Int.max)
                XCTAssertEqual(stream.nextIndex, endIndex)
                
                seek(to: i)
                XCTAssertEqual(stream.skipAndPeek(offset: Int.max), nil)
                XCTAssertEqual(stream.nextIndex, endIndex)
                
                seek(to: i)
                XCTAssertEqual(stream.peek(offset: Int.max), nil)
            }
        }
        checkMoveException()
        
        func subStr(_ offset: Int, n: Int) -> String {
            let srcStartIndex = srcString.startIndex
            return srcString[stream.string.index(srcStartIndex, offsetBy: offset)..<stream.string.index(srcStartIndex, offsetBy: offset+n)]
        }
        func charAtOffset(_ offset: Int) -> Character {
            return srcString[srcString.characters.index(srcString.startIndex, offsetBy: offset)]
        }
        
        let regex = try! NSRegularExpression(pattern: ".*", options: [.dotMatchesLineSeparators])
        
        func checkMatch(offset: Int, count: Int) {
            func checkString(_ str: String, isRealSubset: Bool) {
//                print(str, isRealSubset, offset, count)
                precondition(str.characters.count == count)
                seek(to: offset)
                let index1 = stream.nextIndex
                let index2 = stream.string.index(stream.nextIndex, offsetBy: count, limitedBy: stream.endIndex) ?? stream.endIndex
                let tag1 = stream.stateTag
                let tag2 = tag1 + 1
                
                func checkStream(charsAreSkipped: Bool, line: UInt = #line) {
                    if !charsAreSkipped {
                        XCTAssertEqual(stream.nextIndex, index1, "line: \(line)")
                        XCTAssertEqual(stream.stateTag, tag1, "line: \(line)")
                    } else {
                        XCTAssertEqual(stream.nextIndex, index2, "line: \(line)")
                        if count != 0 { XCTAssertEqual(stream.stateTag, tag2, "line: \(line)") }
                        else { XCTAssertEqual(stream.stateTag, tag1, "line: \(line)") }
                    }
                }
                
                XCTAssertEqual(stream.match(str), isRealSubset)
                checkStream(charsAreSkipped: false)
                
                seek(to: offset)
                XCTAssertEqual(stream.skip(str), isRealSubset)
                seek(to: offset)
                XCTAssertEqual(stream.skip(str.lowercased(), caseInsensitive: true), isRealSubset)
                checkStream(charsAreSkipped: isRealSubset)
                
                if count == 1 {
                    seek(to: offset)
                    XCTAssertEqual(stream.match(str[str.startIndex]), isRealSubset)
                    checkStream(charsAreSkipped: false)
                    
                    seek(to: offset)
                    XCTAssertEqual(stream.skip(str[str.startIndex]), isRealSubset)
                    checkStream(charsAreSkipped: isRealSubset)
                } else if count > 1 {
                    let tag3 = tag1 + (offset == 0 ? 1 : 0)
                    let tag4 = tag3 + 1
                    seek(to: offset)
                    // str은 stream의 substring과 같거나 처음과 마지막 캐릭터만 다르다.
                    let isRestEqual = stream.peek(offset: count - 1) == str[str.characters.index(str.startIndex, offsetBy: count - 1)]
                    seek(to: offset + 1)
                    let index11 = stream.nextIndex
                    let subStr = str[str.characters.index(after: str.startIndex)..<str.characters.index(str.startIndex, offsetBy: count)]
                    XCTAssertEqual(stream.match(subStr), isRestEqual)
                    XCTAssertEqual(stream.stateTag, tag3)
                    
                    seek(to: offset + 1)
                    XCTAssertEqual(stream.skip(subStr), isRestEqual)
                    seek(to: offset + 1)
                    XCTAssertEqual(stream.skip(subStr.lowercased(), caseInsensitive: true), isRestEqual)
                    if isRestEqual {
                        XCTAssertEqual(stream.nextIndex, index2)
                        XCTAssertEqual(stream.stateTag, tag4)
                    } else {
                        XCTAssertEqual(stream.nextIndex, index11)
                        XCTAssertEqual(stream.stateTag, tag3)
                    }
                }
            }
            
            if count == 0 {
                checkString("", isRealSubset: true)
            } else if offset < srcCount {
                func plusOneChar(_ c: Character) -> Character {
                    let sclars = String(c).unicodeScalars
                    return Character(UnicodeScalar(sclars[sclars.startIndex].value + 1)!)
                }
                if offset + count <= srcCount {
                    let ci1 = plusOneChar(charAtOffset(offset))
                    checkString(subStr(offset, n: count), isRealSubset: true)
                    if count == 1 {
                        checkString(String(ci1), isRealSubset: false)
                    } else {
                        checkString("\(ci1)\(subStr(offset+1, n: count-1))", isRealSubset: false)
                        checkString("\(subStr(offset, n: count-1))\(plusOneChar(charAtOffset(offset+count-1)))",
                            isRealSubset: false)
                    }
                } else {
                    checkString("\(subStr(offset, n: srcCount-offset))\(String(repeating: String(charAtOffset(srcCount-1)), count: count-(srcCount-offset)))",
                        isRealSubset: false)
                }
                
                seek(to: offset)
                let index = stream.nextIndex
                let tag = stream.stateTag
                if let matchRange = stream.match(regex)?.range {
                    let mstr = (stream.string as NSString).substring(with: matchRange)
                    XCTAssertEqual(stream.nextIndex, index)
                    XCTAssertEqual(stream.stateTag, tag)
                    XCTAssertTrue(mstr.characters.count >= srcCount-offset)
                    XCTAssertEqual(mstr, subStr(offset, n: mstr.characters.count))
                } else {
                    XCTFail()
                }
            } else {
                let str = String(repeating: String(srcString[srcString.characters.index(before: srcString.endIndex)]), count: count)
                checkString(str, isRealSubset: false)
                
                seek(to: offset)
                let index = stream.nextIndex
                let tag = stream.stateTag
                if let matchRange = stream.match(regex)?.range {
                    XCTAssertEqual((stream.string as NSString).substring(with: matchRange), "")
                } else {
                    XCTFail()
                }
                XCTAssertEqual(stream.nextIndex, index)
                XCTAssertEqual(stream.stateTag, tag)
            }
        }
        for offset in 0...srcCount {
            for count in 0...(srcCount + 15 - offset) {
                checkMatch(offset: offset, count: count)
            }
        }
        
        func checkRead(offset: Int, count: Int) {
            seek(to: offset)
            let index1 = stream.nextIndex
            let index2 = stream.string.index(stream.nextIndex, offsetBy: count, limitedBy: stream.endIndex) ?? stream.endIndex
            let tag1 = stream.stateTag
            let tag2 = tag1 + 1
            
            let str = offset < srcCount ? subStr(offset, n: min(count, srcCount - offset)) : ""
            
            func checkStream(charsAreSkipped: Bool, line: UInt = #line) {
                if !charsAreSkipped {
                    XCTAssertEqual(stream.nextIndex, index1, "line: \(line)")
                    XCTAssertEqual(stream.stateTag, tag1, "line: \(line)")
                } else {
                    XCTAssertEqual(stream.nextIndex, index2, "line: \(line)")
                    if count != 0 { XCTAssertEqual(stream.stateTag, tag2, "line: \(line)") }
                    else { XCTAssertEqual(stream.stateTag, tag1, "line: \(line)") }
                }
            }
            
            XCTAssertEqual(stream.read(count), str)
            checkStream(charsAreSkipped: !str.isEmpty)
            
            seek(to: offset)
            XCTAssertEqual(stream.peek(count: count), str)
            checkStream(charsAreSkipped: false)
            
            seek(to: offset)
            XCTAssertEqual(stream.peek(count: count), str)
            checkStream(charsAreSkipped: false)
            
            if count == 1 {
                seek(to: offset)
                XCTAssertEqual(stream.read(), str.isEmpty ? nil : str[str.startIndex])
            }
            
            seek(to: offset)
            let indexToken = stream.indexToken
            stream.skip(count)
            XCTAssertEqual(stream.read(from: indexToken), str)
            checkStream(charsAreSkipped: indexToken.index(in: stream) != stream.nextIndex)
            
            seek(to: offset)
            let pos1 = stream.position
            let state = stream.state
            stream.skip(count)
            XCTAssertEqual(state.index(in: stream), index1)
            XCTAssertEqual(state.indexToken.index(in: stream), index1)
            XCTAssertEqual(state.position(in: stream), pos1)
            XCTAssertEqual(stream.read(from: state, normalizingNewlines: false), str)
            checkStream(charsAreSkipped: state.index(in: stream) != stream.nextIndex)
        }
        for offset in 0...srcCount {
            for count in 0...(srcCount + 15 - offset) {
                checkRead(offset: offset, count: count)
            }
        }
    }
    
    func testStreams() {
        let srcString = "1234567890ABCDEF"
        let str = " " + srcString
        let stream = CharacterStream(string: str, startIndex: str.characters.index(after: str.startIndex), endIndex: str.endIndex)
        checkBasicCharacterStreamMethods(stream, srcString: srcString)
        
        let string1 = "x"
        let stream1 = CharacterStream(string: string1,
                                 startIndex: string1.characters.index(after: string1.startIndex), endIndex: string1.endIndex)
        checkEmptyStream(stream1)
        
        checkEmptyStream(CharacterStream(string: ""))
    }
    
    func testSkipWhitespaces() {
        let testChars: [Character] = ["\t", "\n", "\r\n", " ", "\u{0008}", "\u{000C}", "\u{0021}"]
        func checkSkipWhitespaces(_ cs: [Character], begin: Int, stream: CharacterStream) {
            let tabStopDistance = 8
            let state0 = stream.state
            let index0 = stream.nextIndex
            
            var line = 1
            var lineBegin = 0
            var i = begin
            var indentation = Int.min
            
            loop: while i < cs.count {
                switch cs[i] {
                case " ":
                    indentation += 1
                case "\t":
                    indentation += tabStopDistance - indentation%tabStopDistance
                case "\r\n", "\n", "\u{000C}":
                    line += 1
                    lineBegin = i + 1
                    indentation = 0
                default:
                    break loop
                }
                i += 1
            }
            
            let tag = state0.tag + (i == begin ? 0 : 1)
            let index = stream.string.characters.index(stream.string.startIndex, offsetBy: i)
            let lineStart = line == 1 ? state0.lineStartIndex : stream.string.characters.index(stream.string.startIndex, offsetBy: lineBegin)
            
            XCTAssertEqual(stream.skipWhitespaces(), i != begin)
            XCTAssertEqual(stream.stateTag, tag)
            XCTAssertEqual(stream.nextIndex, index)
            XCTAssertEqual(stream.lineNumber, line)
            XCTAssertEqual(stream.lineStartIndex, lineStart)
            stream.backtrack(to: state0)
            
            if cs[begin] == "\r\n" || cs[begin] == "\n" || cs[begin] == "\u{000C}" {
                XCTAssertEqual(stream.skipNewlineThenWhitespaces(tabWidth: tabStopDistance), indentation)
                XCTAssertEqual(stream.stateTag, tag)
                XCTAssertEqual(stream.nextIndex, index)
                XCTAssertEqual(stream.lineNumber, line)
                XCTAssertEqual(stream.lineStartIndex, lineStart)
            } else {
                XCTAssertEqual(stream.skipNewlineThenWhitespaces(tabWidth: tabStopDistance), -1)
                XCTAssertEqual(stream.stateTag, state0.tag)
                XCTAssertEqual(stream.nextIndex, index0)
                XCTAssertEqual(stream.lineNumber, state0.lineNumber)
                XCTAssertEqual(stream.lineStartIndex, state0.lineStartIndex)
            }
            stream.backtrack(to: state0)
        }
        
        func checkFastPath() {
            let s = { () -> () -> Character in
                var i = 0
                return {
                    let arr: [Character] = ["\t", " ", "\t", "\t", " ", " "]
                    let ret = arr[i]
                    i = (i + 1) % arr.count
                    return ret
                }
            }()
            let n = { () -> () -> Character in
                var i = 0
                return {
                    let arr: [Character] = ["\n", "\r\n", "\u{000C}", "\n", "\n", "\r\n", "\r\n", "\u{000C}", "\u{000C}"]
                    let ret = arr[i]
                    i = (i + 1) % arr.count
                    return ret
                }
            }()
            let c = { () -> () -> Character in
                var i = 0
                return {
                    let arr: [Character] = ["\u{0008}", "\u{0021}", "\u{0008}", "\u{0008}", "\u{0021}", "\u{0021}"]
                    let ret = arr[i]
                    i = (i + 1) % arr.count
                    return ret
                }
            }()
            let testFuncs = [s, n, c]
            var cs = Array(repeating: Character("_"), count: 11)
            func check1(level: Int) {
                if level > 7 {
                    let str = String(cs)
                    let stream = CharacterStream(string: str,
                        startIndex: str.characters.index(after: str.startIndex),
                        endIndex: str.endIndex)
                    checkSkipWhitespaces(cs, begin: 1, stream: stream)
                } else {
                    for ci in testFuncs {
                        cs[level] = ci()
                        check1(level: level + 1)
                    }
                }
            }
            check1(level: 1)
            
            func check2(level: Int) {
                if level > 10 {
                    let str = String(cs)
                    let stream = CharacterStream(string: str,
                        startIndex: str.characters.index(after: str.startIndex),
                        endIndex: str.endIndex)
                    stream.skip(6)
                    checkSkipWhitespaces(cs, begin: 7, stream: stream)
                    stream.skip(1)
                    checkSkipWhitespaces(cs, begin: 8, stream: stream)
                    stream.skip(1)
                    checkSkipWhitespaces(cs, begin: 9, stream: stream)
                    stream.skip(1)
                    checkSkipWhitespaces(cs, begin: 10, stream: stream)
                } else {
                    for ci in testFuncs {
                        cs[level] = ci()
                        check2(level: level + 1)
                    }
                }
            }
            check2(level: 7)
            
            let str = String(cs)
            let stream = CharacterStream(string: str,
                startIndex: str.characters.index(after: str.startIndex), endIndex: str.endIndex)
            stream.skip(10)
            let tag = stream.stateTag
            XCTAssertFalse(stream.skipWhitespaces())
            XCTAssertEqual(stream.stateTag, tag)
            XCTAssertEqual(stream.nextIndex, stream.string.index(stream.startIndex, offsetBy: 10))
            XCTAssertEqual(stream.lineNumber, 1)
            XCTAssertEqual(stream.lineStartIndex, stream.startIndex)
        }
        checkFastPath()
    }
    
    func testSkipNewlineThenWhitespaces() {
        let stream = CharacterStream(string: "\n\r\t \t  \t   \t ")
        let state0 = stream.state
        XCTAssertEqual(stream.skipNewlineThenWhitespaces(tabWidth: 1), 11)
        stream.backtrack(to: state0)
        XCTAssertEqual(stream.skipNewlineThenWhitespaces(tabWidth: 2), 13)
        stream.backtrack(to: state0)
        XCTAssertEqual(stream.skipNewlineThenWhitespaces(tabWidth: 4), 17)
        
        func checkIndentation(_ str: String, tabWidth: Int, indentation: Int, offset: Int, line: UInt = #line) {
            let stream = CharacterStream(string: str)
            XCTAssertEqual(stream.skipNewlineThenWhitespaces(tabWidth: tabWidth), indentation, "line: \(line)")
            XCTAssertEqual(stream.nextIndex, stream.string.index(stream.startIndex, offsetBy: offset), "line: \(line)")
        }
        checkIndentation("\r\n\t", tabWidth: 100, indentation: 100, offset: 2)
        checkIndentation("\r\n\t ", tabWidth: 200, indentation: 201, offset: 3)
        checkIndentation("\r\n\t\t", tabWidth: 300, indentation: 600, offset: 3)
        checkIndentation("\r\n\t \t", tabWidth: 400, indentation: 800, offset: 4)
        checkIndentation("\r\n\t\t ", tabWidth: 500, indentation: 1001, offset: 4)
        checkIndentation("\r\n\t \t ", tabWidth: 600, indentation: 1201, offset: 5)
    }
    
    func testSkipRestOfLine() {
        let testChars: [Character] = ["\n", "\r\n", "\t", "\u{000C}", "\u{000E}"]
        func checkSkipRestOfLine(_ cs: [Character], begin: Int, stream: CharacterStream) {
            let state0 = stream.state
            
            var i = begin
            while i < cs.count && cs[i] != "\n" && cs[i] != "\r\n" && cs[i] != "\u{000C}" {
                i += 1
            }
            
            let tag = state0.tag + (i == begin ? 0 : 1)
            let index = stream.string.characters.index(stream.string.startIndex, offsetBy: i)
            let str = String(cs[begin..<i])
            
            stream.skipRestOfLine(thenSkippingNewline: false)
            XCTAssertEqual(stream.nextIndex, index)
            XCTAssertEqual(stream.stateTag, tag)
            XCTAssertEqual(stream.lineNumber, state0.lineNumber)
            XCTAssertEqual(stream.lineStartIndex, state0.lineStartIndex)
            stream.backtrack(to: state0)
            
            XCTAssertEqual(stream.readRestOfLine(thenSkippingNewline: false), str)
            XCTAssertEqual(stream.nextIndex, index)
            XCTAssertEqual(stream.stateTag, tag)
            XCTAssertEqual(stream.lineNumber, state0.lineNumber)
            XCTAssertEqual(stream.lineStartIndex, state0.lineStartIndex)
            stream.backtrack(to: state0)
            
            var line2 = state0.lineNumber
            if i < cs.count {
                let c = cs[i]
                if c == "\n" || c == "\r\n" || c == "\u{000C}" {
                    i += 1
                    line2 += 1
                }
            }
            let tag2 = state0.tag + (i == begin ? 0 : 1)
            let index2 = stream.string.characters.index(stream.string.startIndex, offsetBy: i)
            let lineStartIndex2 = line2 == state0.lineNumber ? state0.lineStartIndex : index2
            
            stream.skipRestOfLine(thenSkippingNewline: true)
            XCTAssertEqual(stream.nextIndex, index2)
            XCTAssertEqual(stream.stateTag, tag2)
            XCTAssertEqual(stream.lineNumber, line2)
            XCTAssertEqual(stream.lineStartIndex, lineStartIndex2)
            stream.backtrack(to: state0)
            
            XCTAssertEqual(stream.readRestOfLine(thenSkippingNewline: true), str)
            XCTAssertEqual(stream.nextIndex, index2)
            XCTAssertEqual(stream.stateTag, tag2)
            XCTAssertEqual(stream.lineNumber, line2)
            XCTAssertEqual(stream.lineStartIndex, lineStartIndex2)
            stream.backtrack(to: state0)
        }
        
        func checkFastPath() {
            let s = { () -> Character in
                return "\t"
            }
            let n = { () -> () -> Character in
                var i = 0
                return {
                    let arr: [Character] = ["\n", "\r\n", "\u{000C}", "\n", "\n", "\r\n", "\r\n", "\u{000C}", "\u{000C}"]
                    let ret = arr[i]
                    i = (i + 1) % arr.count
                    return ret
                }
            }()
            let c = { () -> Character in
                return "\u{000E}"
            }
            let testFuncs = [s, n, c]
            var cs = Array(repeating: Character("_"), count: 8)
            func check1(level: Int) {
                if level > 5 {
                    let str = String(cs)
                    let stream = CharacterStream(string: str,
                        startIndex: str.characters.index(after: str.startIndex),
                        endIndex: str.endIndex)
                    checkSkipRestOfLine(cs, begin: 1, stream: stream)
                } else {
                    for ci in testFuncs {
                        cs[level] = ci()
                        check1(level: level + 1)
                    }
                }
            }
            check1(level: 1)
            
            func check2(level: Int) {
                if level > 7 {
                    let str = String(cs)
                    let stream = CharacterStream(string: str,
                        startIndex: str.characters.index(after: str.startIndex),
                        endIndex: str.endIndex)
                    stream.skip(4)
                    checkSkipRestOfLine(cs, begin: 5, stream: stream)
                    stream.skip(1)
                    checkSkipRestOfLine(cs, begin: 6, stream: stream)
                    stream.skip(1)
                    checkSkipRestOfLine(cs, begin: 7, stream: stream)
                } else {
                    for ci in testFuncs {
                        cs[level] = ci()
                        check2(level: level + 1)
                    }
                }
            }
            check2(level: 5)
            
            let str = String(cs)
            let stream = CharacterStream(string: str,
                startIndex: str.characters.index(after: str.startIndex),
                endIndex: str.endIndex)
            stream.skip(7)
            checkSkipRestOfLine(cs, begin: 8, stream: stream)
        }
        checkFastPath()
    }
    
    func testSkipSubstring() {
        func check(_ stream: CharacterStream, cs: [Character], begin: Int, maxLen: Int) {
            let state0 = stream.state
            let tag0 = state0.tag
            let line0 = state0.lineNumber
            let lineStart0 = state0.lineStartIndex
            let index0 = stream.nextIndex
            let lineOffset = state0.lineNumber - 1
            let alwaysTrue = { (_: Character) in true }
            let alwaysFalse = { (_: Character) in false }
            var nTrueN = 0
            let nTrue = { (_: Character) -> Bool in
                if nTrueN > 0 {
                    nTrueN -= 1
                    return true
                } else {
                    return false
                }
            }
            for n in 0...maxLen {
                var line = 1 + lineOffset
                var lineBegin = 0
                var i = begin
                var c = 0
                while c < n && i < cs.count {
                    if cs[i] == "\n" || cs[i] == "\r\n" || cs[i] == "\u{000C}" {
                        line += 1
                        lineBegin = i + 1
                    }
                    i += 1
                    c += 1
                }
                let consumed = c != 0
                let tag = consumed ? tag0 + 1 : tag0
                let index = stream.string.characters.index(stream.string.startIndex, offsetBy: i)
                let containsNewline = line != 1
                let lineStart = !containsNewline ? lineStart0 : stream.string.characters.index(stream.string.startIndex, offsetBy: lineBegin)
                
                func checkStreamAndReset() {
                    XCTAssertEqual(stream.nextIndex, index)
                    XCTAssertEqual(stream.stateTag, tag)
                    XCTAssertEqual(stream.lineNumber, line)
                    XCTAssertEqual(stream.lineStartIndex, lineStart)
                    stream.backtrack(to: state0)
                }
                
                let str = String(cs[begin..<i])
                let normalizedStr = Text.normalizingNewlines(of: str)
                
                if n == 1 {
                    XCTAssertEqual(stream.readCharacter(), c == 0 ? nil : normalizedStr[normalizedStr.startIndex])
                    checkStreamAndReset()
                    XCTAssertEqual(stream.skipNewline(), containsNewline)
                    if containsNewline {
                        checkStreamAndReset()
                    } else {
                        XCTAssertEqual(stream.nextIndex, index0)
                        XCTAssertEqual(stream.stateTag, tag0)
                        XCTAssertEqual(stream.lineNumber, line0)
                        XCTAssertEqual(stream.lineStartIndex, lineStart0)
                    }
                    
                    XCTAssertEqual(stream.skipSubstring(
                        first: alwaysTrue, while: alwaysFalse).count, c)
                    checkStreamAndReset()
                    XCTAssertEqual(stream.readSubstring(
                        normalizingNewlines: false, first: alwaysTrue, while: alwaysFalse),
                        str)
                    checkStreamAndReset()
                    XCTAssertEqual(stream.readSubstring(
                        normalizingNewlines: true, first: alwaysTrue, while: alwaysFalse),
                        normalizedStr)
                    checkStreamAndReset()
                    
                    XCTAssertEqual(stream.skipSubstring(
                        minCount: 0, maxCount: Int.max,
                        first: alwaysTrue, while: alwaysFalse).count,
                        c)
                    checkStreamAndReset()
                    XCTAssertEqual(stream.readSubstring(
                        minCount: 0, maxCount: Int.max, normalizingNewlines: false,
                        first: alwaysTrue, while: alwaysFalse),
                        str)
                    checkStreamAndReset()
                    XCTAssertEqual(stream.readSubstring(
                        minCount: 0, maxCount: Int.max, normalizingNewlines: true,
                        first: alwaysTrue, while: alwaysFalse),
                        normalizedStr)
                    checkStreamAndReset()
                }
                
                XCTAssertEqual(stream.skipSubstring(maxCount: n).count, c)
                checkStreamAndReset()
                XCTAssertEqual(stream.readSubstring(maxCount: n, normalizingNewlines: false), str)
                XCTAssertEqual(stream.read(from: state0, normalizingNewlines: false), str)
                checkStreamAndReset()
                XCTAssertEqual(stream.readSubstring(maxCount: n, normalizingNewlines: true), normalizedStr)
                XCTAssertEqual(stream.read(from: state0, normalizingNewlines: true), normalizedStr)
                checkStreamAndReset()
                
                nTrueN = n
                XCTAssertEqual(stream.skipSubstring(while: nTrue).count, c)
                checkStreamAndReset()
                nTrueN = n
                XCTAssertEqual(stream.readSubstring(normalizingNewlines: false, while: nTrue), str)
                checkStreamAndReset()
                nTrueN = n
                XCTAssertEqual(stream.readSubstring(normalizingNewlines: true, while: nTrue), normalizedStr)
                checkStreamAndReset()
                
                nTrueN = n
                XCTAssertEqual(stream.skipSubstring(minCount: 0, maxCount: Int.max, while: nTrue).count, c)
                checkStreamAndReset()
                nTrueN = n
                XCTAssertEqual(stream.readSubstring(minCount: 0, maxCount: Int.max, normalizingNewlines: false, while: nTrue), str)
                checkStreamAndReset()
                nTrueN = n
                XCTAssertEqual(stream.readSubstring(minCount: 0, maxCount: Int.max, normalizingNewlines: true, while: nTrue), normalizedStr)
                checkStreamAndReset()
                
                let ret1 = stream.skipSubstring(until: "\u{0000}", maxCount: n)
                XCTAssertEqual(ret1.bounds.count, c)
                XCTAssertFalse(ret1.found)
                checkStreamAndReset()
                
                let ret2 = stream.readSubstring(until: "\u{0000}", maxCount: n, normalizingNewlines: false)
                XCTAssertEqual(ret2.bounds.count, c)
                XCTAssertNil(ret2.string)
                checkStreamAndReset()
                
                if i < cs.count && !str.characters.contains(cs[i]) {
                    let cis = String(cs[i])
                    let ret3 = stream.skipSubstring(until: cis, maxCount: n)
                    XCTAssertEqual(ret3.bounds.count, c)
                    XCTAssertTrue(ret3.found)
                    checkStreamAndReset()
                    
                    let ret4 = stream.readSubstring(until: cis, maxCount: n, normalizingNewlines: false)
                    XCTAssertEqual(ret4.bounds.count, c)
                    XCTAssertEqual(ret4.string, str)
                    checkStreamAndReset()
                    
                    let ret5 = stream.readSubstring(until: cis, maxCount: n, normalizingNewlines: true)
                    XCTAssertEqual(ret5.bounds.count, c)
                    XCTAssertEqual(ret5.string, normalizedStr)
                    checkStreamAndReset()
                }
            }
        }
        
        func checkFastPath() {
            let s = { () -> Character in
                return "\t"
            }
            let n = { () -> () -> Character in
                var i = 0
                return {
                    let arr: [Character] = ["\n", "\r\n", "\u{000C}", "\n", "\n", "\r\n", "\r\n", "\u{000C}", "\u{000C}"]
                    let ret = arr[i]
                    i = (i + 1) % arr.count
                    return ret
                }
            }()
            let c = { () -> Character in
                return "\u{000E}"
            }
            let testFuncs = [s, n, c]
            var cs = Array(repeating: Character("_"), count: 10)
            let check2Begin = 6
            let queue = OperationQueue()
            func check1(level: Int) {
                if level > check2Begin {
                    let str = String(cs)
                    let stream = CharacterStream(string: str,
                        startIndex: str.characters.index(after: str.startIndex),
                        endIndex: str.endIndex)
                    let characters = cs
                    if queue.operationCount > 10 {
                        queue.waitUntilAllOperationsAreFinished()
                    }
                    queue.addOperation({
                        check(stream, cs: characters, begin: 1, maxLen: check2Begin)
                    })
                } else {
                    for ci in testFuncs {
                        cs[level] = ci()
                        check1(level: level + 1)
                    }
                }
            }
            check1(level: 1)
            
            func check2(level: Int) {
                if level > cs.count-1 {
                    let str = String(cs)
                    let stream = CharacterStream(string: str,
                        startIndex: str.characters.index(after: str.startIndex),
                        endIndex: str.endIndex)
                    stream.skip(check2Begin-1)
                    let characters = cs
                    if queue.operationCount > 10 {
                        queue.waitUntilAllOperationsAreFinished()
                    }
                    queue.addOperation({
                        for i in check2Begin..<characters.count {
                            check(stream, cs: characters, begin: i, maxLen: characters.count-i)
                            stream.skip()
                        }
                    })
                } else {
                    for ci in testFuncs {
                        cs[level] = ci()
                        check2(level: level + 1)
                    }
                }
            }
            check2(level: check2Begin)
            
            let str = String(cs)
            let stream = CharacterStream(string: str,
                startIndex: str.characters.index(after: str.startIndex),
                endIndex: str.endIndex)
            stream.skip(cs.count-1)
            check(stream, cs: cs, begin: cs.count, maxLen: 1)
            
            queue.waitUntilAllOperationsAreFinished()
        }
        
        func checkArgumentChecking() {
            let count = 10
            let cs = Array(repeating: Character("_"), count: count)
            let css = String(cs)
            let stream = CharacterStream(string: css)
            let alwaysTrue = { (_: Character) in true }
            
            for i in [0, 1, count-1, count] {
                let str = css[css.characters.index(css.startIndex, offsetBy: i)..<css.endIndex]
                let n = count - i
                let beginIndex = stream.string.index(stream.startIndex, offsetBy: i)
                let endIndex = stream.string.index(stream.startIndex, offsetBy: count)
                stream.seek(to: beginIndex)
                XCTAssertEqual(stream.skipSubstring(maxCount: Int.max).count, n)
                XCTAssertEqual(stream.nextIndex, endIndex)
                
                stream.seek(to: beginIndex)
                XCTAssertEqual(stream.readSubstring(maxCount: Int.max, normalizingNewlines: true), str)
                XCTAssertEqual(stream.nextIndex, endIndex)
                
                stream.seek(to: beginIndex)
                XCTAssertEqual(stream.skipSubstring(minCount: -1, maxCount: Int.max, while: alwaysTrue).count, n)
                XCTAssertEqual(stream.nextIndex, endIndex)
                
                stream.seek(to: beginIndex)
                XCTAssertEqual(stream.readSubstring(minCount: -1, maxCount: Int.max,
                    normalizingNewlines: true, while: alwaysTrue),
                    str)
                XCTAssertEqual(stream.nextIndex, endIndex)
            }
        }
        
        func checkMinChars() {
            let cs = "0123456789"
            let stream = CharacterStream(string: cs)
            func isSmall(_ n: Int) -> (Character) -> Bool {
                let sclars = String("0").unicodeScalars
                let c1 = Character(UnicodeScalar(sclars[sclars.startIndex].value + UInt32(n))!)
                return { (c: Character) in c < c1 }
            }
            let alwaysTrue = { (_: Character) in true }
            for n in 0...10 {
                let smaller = isSmall(n)
                XCTAssertEqual(stream.skipSubstring(minCount: n, maxCount: Int.max, while: smaller).count, n)
                XCTAssertEqual(stream.nextIndex, stream.string.index(stream.startIndex, offsetBy: n))
                stream.seek(to: stream.startIndex)
                
                _ = stream.readSubstring(minCount: n, maxCount: Int.max, normalizingNewlines: true, while: smaller)
                XCTAssertEqual(stream.nextIndex, stream.string.index(stream.startIndex, offsetBy: n))
                stream.seek(to: stream.startIndex)
                
                let tag = stream.stateTag
                XCTAssertEqual(stream.skipSubstring(minCount: n + 1, maxCount: Int.max, while: smaller).count, 0)
                XCTAssertEqual(stream.nextIndex, stream.startIndex)
                XCTAssertEqual(stream.stateTag, tag)
                
                _ = stream.readSubstring(minCount: n + 1, maxCount: Int.max, normalizingNewlines: true, while: smaller)
                XCTAssertEqual(stream.nextIndex, stream.startIndex)
                XCTAssertEqual(stream.stateTag, tag)
                
                XCTAssertEqual(stream.skipSubstring(minCount: n + 1, maxCount: n, while: alwaysTrue).count, 0)
                XCTAssertEqual(stream.nextIndex, stream.startIndex)
                XCTAssertEqual(stream.stateTag, tag)
                
                _ = stream.readSubstring(minCount: n + 1, maxCount: n, normalizingNewlines: true, while: alwaysTrue)
                XCTAssertEqual(stream.nextIndex, stream.startIndex)
                XCTAssertEqual(stream.stateTag, tag)
                
                XCTAssertEqual(stream.skipSubstring(minCount: Int.max, maxCount: n, while: alwaysTrue).count, 0)
                XCTAssertEqual(stream.nextIndex, stream.startIndex)
                XCTAssertEqual(stream.stateTag, tag)
                
                _ = stream.readSubstring(minCount: Int.max, maxCount: n, normalizingNewlines: true, while: alwaysTrue)
                XCTAssertEqual(stream.nextIndex, stream.startIndex)
                XCTAssertEqual(stream.stateTag, tag)
            }
        }
        
        checkFastPath()
        checkArgumentChecking()
        checkMinChars()
    }
    
    func testSkipSubstringUntilString() {
        let str = "ABCDEFGHI\tJKLMNOPQR"
        let strLen = str.characters.count
        let stream = CharacterStream(string: str)
        for i0 in 0..<strLen {
            stream.seek(to: stream.string.index(stream.startIndex, offsetBy: i0))
            for i1 in i0..<strLen {
                for n in 1..<(strLen-i1) {
                    func check(_ strToFind: String, maxChars: Int, isPresent: Bool) {
                        let end: Int
                        if isPresent {
                            end = i1
                        } else if maxChars < strLen - i0 {
                            end = i0 + maxChars
                        } else {
                            end = strLen
                        }
                        let skippedStr: String?
                        if isPresent {
                            skippedStr = str[str.characters.index(str.startIndex, offsetBy: i0)..<str.characters.index(str.startIndex, offsetBy: i1)]
                        } else {
                            skippedStr = nil
                        }
                        let state0 = stream.state
                        let ret1 = stream.skipSubstring(until: strToFind, maxCount: maxChars)
                        XCTAssertEqual(ret1.bounds.count, end - i0)
                        XCTAssertEqual(ret1.found, isPresent)
                        XCTAssertEqual(stream.nextIndex, str.characters.index(str.startIndex, offsetBy: end))
                        stream.skip(0)
                        stream.backtrack(to: state0)
                        
                        let ret2 = stream.readSubstring(until: strToFind, maxCount: maxChars, normalizingNewlines: false)
                        XCTAssertEqual(ret2.bounds.count, end - i0)
                        XCTAssertEqual(ret2.string, skippedStr)
                        XCTAssertEqual(stream.nextIndex, str.characters.index(str.startIndex, offsetBy: end))
                        stream.skip(0)
                        stream.backtrack(to: state0)
                    }
                    
                    let strToFind = str[str.characters.index(str.startIndex, offsetBy: i1)..<str.characters.index(str.startIndex, offsetBy: i1+n)]
                    check(strToFind, maxChars: Int.max, isPresent: true)
                    check(strToFind, maxChars: i1-i0, isPresent: true)
                    if i1 - i0 > 0 {
                        check(strToFind, maxChars: i1-i0-1, isPresent: false)
                    }
                    if n > 1 {
                        func plusOneChar(_ c: Character) -> Character {
                            let sclars = String(c).unicodeScalars
                            return Character(UnicodeScalar(sclars[sclars.startIndex].value + 1)!)
                        }
                        var strToNotFind = String(plusOneChar(strToFind[strToFind.startIndex]))
                        strToNotFind += strToFind[strToFind.characters.index(after: strToFind.startIndex)..<strToFind.endIndex]
                        check(strToNotFind, maxChars: Int.max, isPresent: false)
                        var strToNotFind2 = strToFind[strToFind.startIndex..<strToFind.characters.index(strToFind.startIndex, offsetBy: n-1)]
                        strToNotFind2 += String(plusOneChar(strToFind[strToFind.characters.index(strToFind.startIndex, offsetBy: n-1)]))
                        check(strToNotFind2, maxChars: Int.max, isPresent: false)
                    }
                }
            }
        }
    }
    
    func testSubstream() {
        func check(_ stream: CharacterStream) {
            let startIndex0 = stream.startIndex
            let state0 = stream.state
            XCTAssertEqual(stream.readSubstring(maxCount: 3, normalizingNewlines: false), " \r\n0")
            XCTAssertEqual(stream.nextIndex, stream.string.index(startIndex0, offsetBy: 3))
            XCTAssertEqual(stream.lineNumber, 2)
            XCTAssertEqual(stream.lineStartIndex, stream.string.index(startIndex0, offsetBy: 2))
            let state1 = stream.state
            XCTAssertEqual(stream.readSubstring(maxCount: 5, normalizingNewlines: false), "1\n345")
            let substream = stream.substream(from: state1)    // "1\n345"
            stream.backtrack(to: state0)
            let substreamState0 = substream.state
            XCTAssertEqual(substream.startIndex, stream.string.index(startIndex0, offsetBy: 3))
            XCTAssertEqual(substream.endIndex, stream.string.index(startIndex0, offsetBy: 8))
            XCTAssertEqual(substream.lineNumber, 2)
            XCTAssertEqual(substream.lineStartIndex, stream.string.index(startIndex0, offsetBy: 2))
            XCTAssertEqual(substream.readSubstring(maxCount: 10, normalizingNewlines: false), "1\n345")
            XCTAssertEqual(substream.nextIndex, stream.string.index(startIndex0, offsetBy: 8))
            XCTAssertTrue(substream.isAtEndOfStream)
            XCTAssertEqual(substream.lineNumber, 3)
            XCTAssertEqual(substream.lineStartIndex, stream.string.index(startIndex0, offsetBy: 5))
            XCTAssertEqual(stream.readSubstring(maxCount: 10, normalizingNewlines: false), " \r\n01\n3456")
            XCTAssertEqual(stream.nextIndex, stream.string.index(startIndex0, offsetBy: 9))
            XCTAssertEqual(stream.lineNumber, 3)
            substream.backtrack(to: substreamState0)
            XCTAssertEqual(substream.read(), "1")
            
            let substreamState1 = substream.state
            XCTAssertEqual(substream.readSubstring(maxCount: 3, normalizingNewlines: false), "\n34")
            let subSubstream = substream.substream(from: substreamState1) // "\n34"
            XCTAssertEqual(subSubstream.startIndex, stream.string.index(startIndex0, offsetBy: 4))
            XCTAssertEqual(subSubstream.endIndex, stream.string.index(startIndex0, offsetBy: 7))
            XCTAssertEqual(subSubstream.lineNumber, 2)
            XCTAssertEqual(subSubstream.lineStartIndex, stream.string.index(startIndex0, offsetBy: 2))
            XCTAssertEqual(subSubstream.readSubstring(maxCount: 10, normalizingNewlines: false), "\n34")
            XCTAssertEqual(subSubstream.nextIndex, stream.string.index(startIndex0, offsetBy: 7))
            XCTAssertTrue(subSubstream.isAtEndOfStream)
            XCTAssertEqual(subSubstream.lineNumber, 3)
            XCTAssertEqual(subSubstream.lineStartIndex, stream.string.index(startIndex0, offsetBy: 5))
            
            substream.backtrack(to: substreamState1)
            let subSubstream2 = substream.substream(from: substreamState1)
            XCTAssertEqual(subSubstream2.startIndex, stream.string.index(startIndex0, offsetBy: 4))
            XCTAssertEqual(subSubstream2.endIndex, stream.string.index(startIndex0, offsetBy: 4))
            XCTAssertEqual(subSubstream2.lineNumber, 2)
            XCTAssertEqual(subSubstream2.lineStartIndex, stream.string.index(startIndex0, offsetBy: 2))
            XCTAssertTrue(subSubstream2.isAtEndOfStream)
            XCTAssertEqual(substream.readSubstring(maxCount: 10, normalizingNewlines: false), "\n345")
            XCTAssertTrue(substream.isAtEndOfStream)
            
            let substreamStateEnd = substream.state
            let subSubstream3 = substream.substream(from: substreamStateEnd)
            XCTAssertEqual(subSubstream3.startIndex, stream.string.index(startIndex0, offsetBy: 8))
            XCTAssertEqual(subSubstream3.endIndex, stream.string.index(startIndex0, offsetBy: 8))
            XCTAssertEqual(subSubstream3.lineNumber, 3)
            XCTAssertEqual(subSubstream3.lineStartIndex, stream.string.index(startIndex0, offsetBy: 5))
            XCTAssertTrue(subSubstream3.isAtEndOfStream)
            
            stream.skip(-1)
            let state2 = stream.state
            XCTAssertEqual(stream.read(), "6")
            XCTAssertTrue(stream.isAtEndOfStream)
            
            let substream2 = stream.substream(from: state2)
            XCTAssertEqual(substream2.startIndex, stream.string.index(startIndex0, offsetBy: 8))
            XCTAssertEqual(substream2.endIndex, stream.string.index(startIndex0, offsetBy: 9))
            XCTAssertEqual(substream2.nextIndex, stream.string.index(startIndex0, offsetBy: 8))
            XCTAssertEqual(substream2.lineNumber, 3)
            XCTAssertEqual(substream2.lineStartIndex, stream.string.index(startIndex0, offsetBy: 5))
            XCTAssertEqual(substream2.read(), "6")
            XCTAssertTrue(substream2.isAtEndOfStream)
            
            let stateEnd = stream.state
            let substream3 = stream.substream(from: stateEnd)
            XCTAssertEqual(substream3.startIndex, stream.string.index(startIndex0, offsetBy: 9))
            XCTAssertEqual(substream3.endIndex, stream.string.index(startIndex0, offsetBy: 9))
            XCTAssertEqual(substream3.nextIndex, stream.string.index(startIndex0, offsetBy: 9))
            XCTAssertEqual(substream3.lineNumber, 3)
            XCTAssertEqual(substream3.lineStartIndex, stream.string.index(startIndex0, offsetBy: 5))
            XCTAssertTrue(substream3.isAtEndOfStream)
            
            stream.backtrack(to: state1)
            let substream4 = stream.substream(from: state1)
            XCTAssertEqual(substream4.startIndex, stream.string.index(startIndex0, offsetBy: 3))
            XCTAssertEqual(substream4.endIndex, stream.string.index(startIndex0, offsetBy: 3))
            XCTAssertEqual(substream4.nextIndex, stream.string.index(startIndex0, offsetBy: 3))
            XCTAssertEqual(substream4.lineNumber, 2)
            XCTAssertEqual(substream4.lineStartIndex, stream.string.index(startIndex0, offsetBy: 2))
            XCTAssertTrue(substream4.isAtEndOfStream)
        }
        let str1 = "! \r\n01\n3456!"
        let str2 = "! \n\n01\n3456!"
        XCTAssertEqual(str1.characters.count + 1, str2.characters.count)
        let stream = CharacterStream(string: str1,
            startIndex: str1.characters.index(after: str1.startIndex),
            endIndex: str1.characters.index(before: str1.endIndex))
        check(stream)
    }
}



