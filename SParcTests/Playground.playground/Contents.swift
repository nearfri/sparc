//: Playground - noun: a place where people can play

import Cocoa
import SParc
import CoreMedia


let str = "abcdefghijklmnopqrstuvwxyz"
let stream = CharacterStream(string: str)
let p1 = string(until: "xy", caseInsensitive: false, maxCount: .max, skippingString: true)
let p2 = restOfLine()
let p3 = p1 !>>! p2
let ret = p3.run(str)
print(ret)





